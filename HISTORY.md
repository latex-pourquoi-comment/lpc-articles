Historique du projet
====================

*Date de création*: 2017/06/11


Raisons de mise en place
------------------------
L'idée de ce projet est née à la suite d'une discussion tenue sur la liste de discussion gut(chez)ens.fr.

Le 6 juin 2017, <.PlacerLeNomSiAccord.>, lance un fil de discussion "Projet d'article pour Livres Hebdo".
S'ensuivent de nombreux échanges et la production par l'initiateur d'un texte méritant d'être publié.




Événements importants
---------------------

### 2017/06
Création du projet et mise en place du dépôt



[comment]: # ( Local Variables: )
[comment]: # ( coding: utf-8	)
[comment]: # ( End:		)
