%% Format latex public pour les CG
\NeedsTeXFormat{LaTeX2e}% LaTeX 2.09 can't be used (nor non-LaTeX)
[1995/06/01]% LaTeX date must be June 1995 or later
\ProvidesClass{cedram-cg}[2017/10/16 v0.2]
%% Option langue
\newif\if@francais\@francaisfalse
\newif\ifcdr@chapters\cdr@chaptersfalse
\DeclareOption{french}{\@francaistrue}
\DeclareOption{anglais}{\@francaisfalse}
\DeclareOption{article}{\cdr@chaptersfalse}
\DeclareOption{chapters}{\cdr@chapterstrue}
\ExecuteOptions{french,article}
\ProcessOptions\relax
%% On charge amsart
\LoadClass[centertags,leqno,portrait,10pt,twoside,%
 onecolumn]{amsart}[2004/08/06 v2.20]
%% Modifs amsart pour compatibilit� cedram
\AtBeginDocument{%
  \@for\@tempa:=-1,0,1,2,3,4\do{%
    \@ifundefined{r@tocindent\@tempa}{%
      \@xp\gdef\csname r@tocindent\@tempa\endcsname{0pt}}{}%
  }%
}
\def\@writetocindents{%
  \begingroup
  \@for\@tempa:=-1,0,1,2,3\do{%
    \immediate\write\@auxout{%
      \string\newlabel{tocindent\@tempa}{%
        \csname r@tocindent\@tempa\endcsname}}%
  }%
  \endgroup}
%% Structure de la commande \author
\renewcommand{\author}[2][]{%
  \ifx\@empty\authors
    \gdef\authors{#2}%
    \g@addto@macro\addresses{\author{#2}}%
  \else
    \g@addto@macro\authors{\and#2}%
    \g@addto@macro\addresses{\author{#2}}%
  \fi
  \@ifnotempty{#1}{%
    \ifx\@empty\shortauthors
      \gdef\shortauthors{#1}%
    \else
      \g@addto@macro\shortauthors{\and#1}%
    \fi
  }%
}
\edef\author{\@nx\@dblarg
  \@xp\@nx\csname\string\author\endcsname}
%% Th�or�mes pr�d�finis (piqu� � smfthm)
\newif\ifcdr@thmsin\cdr@thmsintrue
\newif\ifcdr@thmsas\cdr@thmsasfalse
\newif\ifcdr@thm@necounter\cdr@thm@necounterfalse
\def\OneNumberAllTheorems{\cdr@thm@necountertrue}
\def\NumberTheoremsIn#1{\@ifempty{#1}%
     {\cdr@thmsinfalse}%
     {\@ifundefined{c@#1}{\@nocounterr{#1}}%
       {\cdr@thmsintrue\cdr@thmsasfalse\def\@NumberTheorems{#1}}}}
  \NumberTheoremsIn{section}%
\def\NumberTheoremsAs#1{\@ifempty{#1}%
     {\cdr@thmsasfalse}%
     {\@ifundefined{c@#1}{\@nocounterr{#1}}%
       {\cdr@thmsinfalse\cdr@thmsastrue\cdr@thm@necountertrue\def\@NumberTheorems{#1}}}}
  \NumberTheoremsAs{}%
\def\SwapTheoremNumbers{\def\thm@swap{S}}
\def\NoSwapTheoremNumbers{\def\thm@swap{N}}
  \NoSwapTheoremNumbers
\def\cdr@thmdefs{%
  \theoremstyle{plain}
  \ifcdr@thm@necounter
  \ifcdr@thmsin
  \newcounter{cdrthm}[\@NumberTheorems]%
  \xdef\thecdrthm
         {\expandafter\noexpand\csname the\@NumberTheorems\endcsname
           \@thmcountersep\@thmcounter{cdrthm}}%
    \gdef\cdr@thm{cdrthm}%     
    \else
    \ifcdr@thmsas
    \xdef\cdr@thm{\@NumberTheorems}%
    \else
    \newcounter{cdrthm}%
    \gdef\cdr@thm{cdrthm}%     
    \fi\fi\fi
\ifcdr@thm@necounter
  \newtheorem{theo}[\cdr@thm]{\theoname}%
  \newtheorem{prop}[\cdr@thm]{\propname}%
  \newtheorem{conj}[\cdr@thm]{\conjname}%
  \newtheorem{coro}[\cdr@thm]{\coroname}%
  \newtheorem{lemm}[\cdr@thm]{\lemmname}%
  \theoremstyle{definition}%
  \newtheorem{defi}[\cdr@thm]{\definame}%
  \theoremstyle{remark}%
  \newtheorem{nota}[\cdr@thm]{\notaname}%
  \newtheorem{notas}[\cdr@thm]{\notasname}%
  \newtheorem{rema}[\cdr@thm]{\remaname}%
  \newtheorem{remas}[\cdr@thm]{\remasname}%
  \newtheorem{exem}[\cdr@thm]{\exemname}%
  \newtheorem{exems}[\cdr@thm]{\exemsname}%
\else
\ifcdr@thmsin
  \newtheorem{theo}{\theoname}[\@NumberTheorems]%
  \newtheorem{prop}{\propname}[\@NumberTheorems]%
  \newtheorem{conj}{\conjname}[\@NumberTheorems]%
  \newtheorem{coro}{\coroname}[\@NumberTheorems]%
  \newtheorem{lemm}{\lemmname}[\@NumberTheorems]%
  \theoremstyle{definition}%
  \newtheorem{defi}{\definame}[\@NumberTheorems]%
  \theoremstyle{remark}%
  \newtheorem{nota}{\notaname}[\@NumberTheorems]%
  \newtheorem{notas}{\notasname}[\@NumberTheorems]%
  \newtheorem{rema}{\remaname}[\@NumberTheorems]%
  \newtheorem{remas}{\remasname}[\@NumberTheorems]%
  \newtheorem{exem}{\exemname}[\@NumberTheorems]%
  \newtheorem{exems}{\exemsname}[\@NumberTheorems]%
\else
  \newtheorem{theo}{\theoname}%
  \newtheorem{prop}{\propname}%
  \newtheorem{conj}{\conjname}%
  \newtheorem{coro}{\coroname}%
  \newtheorem{lemm}{\lemmname}%
  \theoremstyle{definition}%
  \newtheorem{defi}{\definame}%
  \theoremstyle{remark}%
  \newtheorem{nota}{\notaname}%
  \newtheorem{notas}{\notasname}%
  \newtheorem{rema}{\remaname}%
  \newtheorem{remas}{\remasname}%
  \newtheorem{exem}{\exemname}%
  \newtheorem{exems}{\exemsname}%
\fi\fi
  \theoremstyle{definition}%
  \newtheorem*{defi*}{\definame}%
  \theoremstyle{remark}%
  \newtheorem*{nota*}{\notaname}%
  \newtheorem*{notas*}{\notasname}%
  \newtheorem*{rema*}{\remaname}%
  \newtheorem*{remas*}{\remasname}%
  \newtheorem*{exem*}{\exemname}%
  \newtheorem*{exems*}{\exemsname}%
  \theoremstyle{plain}%
  \newtheorem*{theo*}{\theoname}%
  \newtheorem*{prop*}{\propname}%
  \newtheorem*{conj*}{\conjname}%
  \newtheorem*{coro*}{\coroname}%
  \newtheorem*{lemm*}{\lemmname}%
}
\def\cdr@enoncedef{%
 \newenvironment{enonce}[2][plain]%
    {\let\cdrenonce\relax \theoremstyle{##1}%
      \ifcdr@thm@necounter
      \newtheorem{cdrenonce}[\cdr@thm]{##2}%
      \else
      \ifcdr@thmsin
      \newtheorem{cdrenonce}{##2}[\@NumberTheorems]%
      \else
      \newtheorem{cdrenonce}{##2}%
      \fi\fi
     \begin{cdrenonce}}%
    {\end{cdrenonce}}%
 \newenvironment{enonce*}[2][plain]%
    {\let\cdrenonce\relax \theoremstyle{##1}%
     \newtheorem*{cdrenonce}{##2}%
     \begin{cdrenonce}}%
    {\end{cdrenonce}}%
}
\AtBeginDocument{%
\cdr@thmdefs
\cdr@enoncedef}
\def\propname{Proposition}%
\def\conjname{Conjecture}%
\def\notaname{Notation}%
\def\notasname{Notations}%
\if@francais
\def\theoname{Th\'eor\`eme}%
\def\coroname{Corollaire}%
\def\lemmname{Lemme}%
\def\definame{D\'efinition}%
\def\remaname{Remarque}%
\def\remasname{Remarques}%
\def\exemname{Exemple}%
\def\exemsname{Exemples}%
\else
\def\theoname{Theorem}%
\def\coroname{Corollary}%
\def\lemmname{Lemma}%
\def\definame{Definition}%
\def\remaname{Remark}%
\def\remasname{Remarks}%
\def\exemname{Example}%
\def\exemsname{Examples}%
\fi
%%�equalenv d�finit env1 �quivalent � env2 (suppos� pr�d�fini)
%% si env1 est d�j� d�fini, ne change rien
\def\equalenv#1#2{%
\@ifundefined{#1}%
{\@xp\let\csname #1\@xp\endcsname\csname #2\endcsname
\@xp\let\csname end#1\@xp\endcsname\csname end#2\endcsname}
{\message{#1 already defined: I won't redefine it!^^J}}}
\equalenv{theorem}{theo}
\equalenv{thm}{theo}
\equalenv{thm*}{theo*}
\equalenv{lemma}{lemm}
%% M�tadonn�es
% titre du journal 
\newcommand{\setpublname}[2][Titre abr\'eg\'e absent]{%
\gdef\shortpublname{#1}\gdef\publname{#2}}
\setpublname{Cahiers GUTenberg}%
\def\currentjournaltitle{Cahiers GUTenberg}
% entit� �ditrice du journal 
\edef\currentjournalpublisher{Association GUTenberg}
% titre abr�g� du journal
\def\currentjournalshorttitle{Cah. GUT}
% acronyme
\def\cdr@journalacro{CG}
% ISSN
\def\@issn{1140-9304}
%
\def\journalURL{http://www.gutenberg.eu.org/publications/cahiers/}
\def\CDRlegalURL{http://www.gutenberg.eu.org/publications/22-avertissements.html}
%\CDRjournalURL{cg}
%
%% g�om�tries
\paperwidth=165mm
\paperheight=225mm
% 
\textwidth=105mm
\newdimen\Textwidth\Textwidth125mm
\ifx\undefined\pdfpagewidth\else
\pdfpagewidth\paperwidth
\pdfpageheight\paperheight
\fi
\topmargin-5.4mm
\oddsidemargin-5.4mm
\evensidemargin14.6mm
\headheight12pt
\headsep-12pt
\topskip14pt
\normaltopskip14pt
\global\normalbaselineskip=12.5pt
\textheight=\topskip
\advance\textheight36\normalbaselineskip
%% Typo
\csname url@samestyle\endcsname
%%�param�tres C&J pour Utopia
\pretolerance=800
\tolerance=1800
\emergencystretch1em
%% Soyons plus souples et corrigeons � la main !
\widowpenalty=3000
\clubpenalty=3000
\brokenpenalty=3000
\footskip48pt
\def\footnoterule{\relax}
\skip\footins=18pt plus 4pt minus 4pt
\parskip=0pt
\parindent=1em
\normalparindent=1em
\renewcommand\baselinestretch{}
\smallskipamount=.5\normalbaselineskip minus .5pt
\medskipamount=1\normalbaselineskip  minus .6pt
\bigskipamount=2\normalbaselineskip plus 1pt minus 1pt
\frenchspacing
%% Utopia-Fourier
\RequirePackage{lmodern}
\def\verbatim@font{\normalfont\verbatimfontsize\ttfamily}
\def\verbatimfontsize{\normalsize}
\def\displayverbatimfontsize{\small}
\RequirePackage[%expert,
   upright]{fourier}
\def\TeX{T\kern-.18em\lower.5ex\hbox{E}\kern-.06emX\@}
\DeclareRobustCommand{\LaTeX}{L\kern-.26em%
        {\sbox\z@ T%
         \vbox to\ht\z@{\hbox{\check@mathfonts
                              \fontsize\sf@size\z@
                              \math@fontsfalse\selectfont
                              A}%
                        \vss}%
        }%
        \kern-.1em%
        \TeX}
\DeclareRobustCommand{\LaTeXe}{\mbox{\m@th
  \if b\expandafter\@car\f@series\@nil\boldmath\fi
  \LaTeX\kern.15em2$_{\textstyle\varepsilon}$}}
%% M�tadonn�es article
\let\@altabstract\@empty
\let\@alttitle\@empty
\renewcommand*{\title}[2][]{\gdef\shorttitle{#1}\gdef\@shorttitle{#1}\gdef\@title{#2}}
\edef\title{\@nx\@dblarg
  \@xp\@nx\csname\string\title\endcsname}
\newcommand*{\alttitle}[1]{\gdef\@alttitle{#1}}%
\newcommand*{\altkeywords}[1]{\gdef\@altkeywords{#1}}%
\newbox\altabstractbox
\newenvironment{altabstract}{%
  \ifx\maketitle\relax
    \ClassWarning{\@classname}{Altabstract should precede
      \protect\maketitle\space in AMS document classes; reported}%
  \fi
  \global\setbox\altabstractbox=\vtop \bgroup
    \normalfont\Small
    \list{}{\labelwidth\z@
      \leftmargin3pc \rightmargin\leftmargin
      \listparindent\normalparindent \itemindent\z@
      \parsep\z@ \@plus\p@
      \let\fullwidthdisplay\relax
    }%
    \item[\hskip\labelsep\scshape\altabstractname.]%
}{%
  \endlist\egroup
  \ifx\@setaltabstract\relax \@setaltabstracta \fi
}
\def\@setaltabstract{\@setaltabstracta \global\let\@setaltabstract\relax}
\def\@setaltabstracta{%
\ifx\@empty\@alttitle\else
\begin{center}%
\large\slshape  \@alttitle  
\end{center}%
\fi
  \ifvoid\altabstractbox
  \else
%     \skip@20\p@ \advance\skip@-\lastskip
%     \advance\skip@-\baselineskip \vskip\skip@
    \box\altabstractbox
    \prevdepth\z@ % because \altabstractbox is a vtop
  \fi
}
\def\@setabstract{\@setabstracta \global\let\@setabstract\relax
\@setaltabstract
 \ifx\@empty\thankses\else\@setthanks\fi}
\def\abstractheadfont{\scshape}
%% 10 pt et dimensions TeX...
\def\@mainsize{10}\def\@ptsize{0}%
  \def\@typesizes{%
    \or{5}{6.5}% Tiny
    \or{6}{8}% tiny
    \or{7}{9}% SMALL = scriptsize
    \or{8.6}{11}% Small = footnotesize
    \or{9.2}{11.4}% small
    \or{10}{12.5}% normalsize
    \or{11}{13.7}% large
    \or{12}{14.4}% Large
    \or{14}{17}% LARGE
    \or{17}{20}% huge
    \or{20}{24}% Huge
}%
\normalsize \linespacing=\baselineskip
\let\ept\Small
%%%% Versions jolies -- macros sp�cifiques
\RequirePackage{amssymb}
\let\le\leqslant
\let\ge\geqslant 
\let\leq\leqslant   
\let\geq\geqslant
\def\hb{\hfill\break}
%%
\def\todo{� faire !}
\def\pointir{\discretionary{.}{}{.\kern.3em---\kern.4em\ignorespaces}}
\def\threestars{%
  \bigskip
  \null
  \begin{center}%
    \Large
    *\\[.2em]
  *\hspace*{1.2em}*
  \end{center}%
  \bigskip
}
\def\Par{\vspace{\baselineskip}\@par}
\def\thsp{\if@francais\,\fi}
% Pas de mise en romain des num�rotations intempestives !
\def\@upn#1{#1}
%% Macros pratiques
\def\tn#1{\text{\normalfont#1}}
\newcommand\nfrac[2]{#1/#2}
\newcommand\pfrac[2]{(#1/#2)}
%% Param�tres de mise en page
%%�1re page
\pagenumbering{arabic}%
%% Mention de copyright
\def\@setcopyright{}%
%% Style des pages courantes
\unitlength1mm
\def\ps@cg{%\ps@empty
  \def\@evenhead{%
    \def\thanks{\protect\thanks@warning}%
  }%
  \def\@oddhead{%
    \def\thanks{\protect\thanks@warning}%
  }%
  \def\@evenfoot{%
    \begin{picture}(0,0)
       % croix du bas
      \put(-20,-4){\rule[-10mm]{.4bp}{20mm}}%
      \put(-30,-4){\rule[-.2bp]{20mm}{.4bp}}%
%%       % croix du haut
%%       \put(106,178){\rule[-12mm]{.4bp}{20mm}}%
%%       \put(98,178){\rule[-.2bp]{16mm}{.4bp}}%
      \put(-16,0){\normalfont\itshape Cahiers GUTenberg \no
        \currentissue{} --- \currentmonth\ \currentyear\hfill}%
      \put(-22.5,-11){\makebox[0pt][r]{\huge\thepage}}%
    \end{picture}%
  }
  \def\@oddfoot{%
    \hfill
     \begin{picture}(0,0)
       % croix du bas
      \put(20,-4){\rule[-10mm]{.4bp}{20mm}}
      \put(10,-4){\rule[-.2bp]{20mm}{.4bp}}
%%       % croix du haut
%%       \put(-106,178){\rule[-12mm]{.4bp}{20mm}}
%%      \put(-114,178){\rule[-.2bp]{16mm}{.4bp}}
      \put(16,0){\makebox[0pt][r]{\normalfont\itshape\let\\\space \@shorttitle}}
      \put(22.5,-11){\makebox[0pt][l]{\huge\thepage}}%
    \end{picture}%
  }
}%
\let\ps@plain\ps@cg
\pagestyle{cg}%
%% Style de la premi�re page
\def\ps@firstpage{\ps@plain
  \def\@oddfoot{%
    \normalfont\itshape Cahiers GUTenberg \no
        \currentissue{} --- \currentmonth\ \currentyear, p.~\pageinfo.\hfill
    \global\topskip\normaltopskip}%
  \let\@evenfoot\@oddfoot
  \def\@oddhead{%
    \hfill
    \begin{picture}(0,0)% croix du haut
    \put(20,3){\rule[-10mm]{.4bp}{20mm}}%
    \put(10,3){\rule[-.2bp]{20mm}{.4bp}}%
    \put(20,3){\circle{4}}%
  \end{picture}%
}%
\let\@evenhead\@oddhead % in case an article starts on a left-hand page
}%
%% Identificiation article premi�re page
\def\@logofont{\Small}
\def\article@logo{%
  \set@logo{%
    {\currentjournaltitle}\hfill
    \ifx\@empty\currentvolume \textbf{Version de travail}
    \else Volume~{\@Roman\currentvolume}, \no\currentissue%
, \currentyear\\
    \strut\hfill p.~\pageinfo\par\fi
  }%
}
%% Pr�sentation du titre
\def\@settitle{%
  \bgroup
  \raggedright\rightskip\@bigflushglue
%  \vglue2mm
  \normalfont\Huge%\titleshape
  \uppercasenonmath\@title
  \noindent
  \begin{hyphenrules}{farsi}%
  \aldineleft\kern.5em\@title\unskip\@@par
\end{hyphenrules}%
\egroup
}
\def\@setsubtitle{%
  \bgroup	
  \raggedright
  \vglue8pt
  \normalfont\LARGE%\titleshape
  \uppercasenonmath\@subtitle
  \noindent
  \begin{hyphenrules}{farsi}%
  \@subtitle\unskip\@@par
\end{hyphenrules}%
  \egroup
}
%% Pr�sentation des auteurs
\def\author@andify{%
  \nxandlist {\unskip ,\penalty-2 \space\ignorespaces}%
  {\unskip ,\penalty-2 \space\ignorespaces}%
  {\unskip ,\penalty-2 \space\ignorespaces}%
}
\let\nobreakauthor~
\def\initial#1{#1}
\def\lastname#1{#1}
\def\firstname#1{#1}
\def\middlename#1{#1}
\def\vonname#1{#1}
\newcount\cdr@thankcount
\cdr@thankcount0
\def\@setauthors{%
  \begingroup
  \def\thanks{\protect\thanks@warning}%
  \raggedright
  \Large%\scshape \@topsep10\p@\relax
  \def\lastname##1{\bsc{##1}}%
  \def\firstname##1{\mbox{##1}}%
  \author@andify\authors
  \def\\{\protect\linebreak}%
  \unskip\kern.5em\raisebox{0pt}{\oldpilcrowone}\kern.35em\authors
  \vskip16pt
  \endgroup
}
\def\@setdedicatory{\normalfont\noindent\hskip.15\columnwidth\begingroup\hsize=.85\columnwidth
    \vtop{\raggedleft{\Small\em\@dedicatory\@@par}}\endgroup\vskip6pt}
%% Derni�re page
\newdimen\authwidth\authwidth.45\textwidth
\newcount\auth@count
\auth@count0
\newcount\noauth@count
\noauth@count0
\def\@setaddresses{\par
  \begingroup
  \def\lastname##1{\bsc{##1}}%
  \def\firstname##1{\mbox{##1}}%
  \def\author##1{\ifcase\auth@count
    \def\tab@@what{}\or
    \def\tab@@what{&}\or
    \def\tab@@what{\tabularnewline[.4\baselineskip]}\or
    \def\tab@@what{&}\or
    \def\tab@@what{\tabularnewline[.4\baselineskip]}\or
    \def\tab@@what{&}\or
    \def\tab@@what{\tabularnewline[.4\baselineskip]}\fi
	\tab@@what
%	\message{**\the\auth@count**}
    \global\advance\auth@count1\relax
    \@ifnotempty{##1}{{\smaller\oldpilcrowtwo}\kern.35em\ignorespaces##1\unskip\\}}%
  \def\noauthor##1##2{\ifcase\noauth@count
    \def\tab@@what{}\or
    \def\tab@@what{&}\or
    \def\tab@@what{\tabularnewline[.4\baselineskip]}\or
    \def\tab@@what{&}\or
    \def\tab@@what{\tabularnewline[.4\baselineskip]}\or
    \def\tab@@what{&}\or
    \def\tab@@what{\tabularnewline[.4\baselineskip]}\fi
	\tab@@what
%	\message{**\the\auth@count**}
    \global\advance\noauth@count1\relax
    \@ifnotempty{##2}{{\smaller\oldpilcrowtwo}\kern.35em\ignorespaces##2\unskip\\}}%
   \def\address##1##2{%
     \@ifnotempty{##1}{(\ignorespaces##1\unskip)\\}%
     \ignorespaces##2\unskip\\}%
 \def\curraddr##1##2{%
     \@ifnotempty{##2}{\curraddrname{}%
       \@ifnotempty{##1}{(\ignorespaces##1\unskip)}:\\
       ##2\\}}%
   \def\email##1##2{%
     \@ifnotempty{##2}{%
       \href{mailto:##2}{##2}\\}}%
   \def\urladdr##1##2{%
%     \def~{\char`\~}%
     \@ifnotempty{##2}{%\nobreak\urladdrname
%       \@ifnotempty{##1}{, \ignorespaces##1\unskip}\/:\space
       \url{##2}\\}}%
   \footnotesize
   \begin{longtable}[t]{*{2}{>{\raggedright}p{\authwidth}}}
     \addresses
   \end{longtable}
   \endgroup}%
%%�final
\RequirePackage{array,longtable}
\def\enddoc@text{%
      \begingroup
 \def\nobreakauthor{\relax}%%
 \def\and{\relax}%%
   \def\firstname##1{\relax}%
    \def\lastname##1{\relax}%
    \def\middlename##1{\relax}%
    \def\vonname##1{\relax}%
    \def\junior##1{\relax}%
    \edef\@tmpa{\authors}
    \fontdimen2\font=0pt\fontdimen3\font=0pt\fontdimen4\font=0pt\relax
  \sbox\@tempboxa{\relax\@tmpa}%
  \ifdim\wd\@tempboxa>\z@%
  \ClassError{\@classname}{%
   No printable char should be placed outside of name commands for
   authors names (firstname, lastname, middlename, vonname,
   junior). If you want to insert a nonbreakable space, please use nobreakauthor!
      }{%
RTFM!
      }%
\fi
\endgroup
\normalfont\footnotesize
\nobreak\addvspace{6mm}\parindent=0pt\parskip0pt\relax
\ifx\@empty\addresses \else\@setaddresses\fi\@@par}
\def\printauthors{\xdef\enddoc@text{}\normalfont\footnotesize
\nobreak\addvspace{6mm}\parindent=0pt\parskip0pt\relax
\ifx\@empty\addresses \else\@setaddresses\fi\@@par}
\def\@adminfootnotes{%
% ne fait rien, pourrait imprimer les mots cl�s... Mais a priori �a
% marche mieux avec Note...
  \begingroup%\parindent=0pt\normalparindent=0pt
  \let\@makefnmark\relax  \let\@thefnmark\relax
                                %  \ifx\@empty\@keywords\else \@footnotetext{\@setkeywords}\fi
                                % \ifx\@empty\@date\else \@footnotetext{\@setdate}\fi
\endgroup}
\def\@setthanks{{ \normalfont\Small
    \list{}{\labelwidth\z@
      \leftmargin3pc \rightmargin\leftmargin
      \listparindent\normalparindent \itemindent\z@
      \parsep\z@ \@plus\p@
      \let\fullwidthdisplay\relax
    }%
    \item[\hskip\labelsep\scshape\textsc{Note}.]%
%\@@par\noindent \textsc{Note}\pointir\ignorespaces
      \let\Par\relax\def\thanks##1{\Par##1\@addpunct.\def\Par{\item[]\hskip\normalparindent}}%
  \thankses\@@par\endlist}}% 
\def\@setsubjclass{%
  {\slshape\subjclassname\thsp:}\space\@subjclass\@addpunct.}
\def\@setkeywords{%
  {\slshape \keywordsname\thsp:}\space \@keywords\@addpunct.}
\if@francais
 \def\keywordsname{Mots-cl�s}
 \def\subjclassname{Classification math.}
\renewcommand{\abstractname}{R\'esum\'e}
\newcommand{\altabstractname}{Abstract}
\else
 \def\keywordsname{Keywords}
 \def\subjclassname{Math. classification}
\newcommand{\altabstractname}{R\'esum\'e}
\fi
\@xp\let\csname subjclassname@2000\endcsname \subjclassname
%% Dates
\def\@setdaterecieved{%
%  \par
  Re�u le \@daterecieved, }%
\def\@setdaterevised{%
%  \newline
  r�vis� le \@daterevised, }%
\def\@setdateaccepted{%
%  \newline
  accept� le \@dateaccepted.}%
%
%%�final
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% sections
\ifcdr@chapters
\newcounter{chapter}
%   \def\chaptermark{%
%     \@secmark\markboth\chapterrunhead{}}%
\renewcommand\thechapter       {\arabic{chapter}}
\let\chapterrunhead\partrunhead
\let\sectionrunhead\partrunhead
\def\chapter{%
  {\pagestyle{empty}\cleardoublepage}
  \thispagestyle{plain}\global\@topnum\z@
  \@afterindenttrue \secdef\@chapter\@schapter}
\def\@chapter[#1]#2{\refstepcounter{chapter}%
  \ifnum\c@secnumdepth<\z@ \let\@secnumber\@empty
  \else \let\@secnumber\thechapter \fi
  \typeout{\chaptername\space\@secnumber}%
  \def\@toclevel{0}%
  \ifx\chaptername\appendixname \@tocwriteb\tocappendix{chapter}{#2}%
  \else \@tocwriteb\tocchapter{chapter}{#2}\fi
%  \chaptermark{#1}%
  \addtocontents{lof}{\protect\addvspace{10\p@}}%
  \addtocontents{lot}{\protect\addvspace{10\p@}}%
  \@makechapterhead{#2}\@afterheading}
\def\@schapter#1{\typeout{#1}%
  \let\@secnumber\@empty
  \def\@toclevel{0}%
  \ifx\chaptername\appendixname \@tocwriteb\tocappendix{chapter}{#1}%
  \else \@tocwriteb\tocchapter{chapter}{#1}\fi
%  \chaptermark{#1}%
  \addtocontents{lof}{\protect\addvspace{10\p@}}%
  \addtocontents{lot}{\protect\addvspace{10\p@}}%
  \@makeschapterhead{#1}\@afterheading}
\newcommand\chaptername{Chapter}
\def\@makechapterhead#1{\global\topskip 7.5pc\relax
  \begingroup
  \fontsize{\@xivpt}{18}\bfseries\centering
    \ifnum\c@secnumdepth>\m@ne
      \leavevmode \hskip-\leftskip
      \rlap{\vbox to\z@{\vss
          \centerline{\normalsize\mdseries
              \uppercase\@xp{\chaptername}\enspace\thechapter}
          \vskip 3pc}}\hskip\leftskip\fi
     #1\par \endgroup
  \skip@34\p@ \advance\skip@-\normalbaselineskip
  \vskip\skip@ }
\def\@makeschapterhead#1{\global\topskip 7.5pc\relax
  \begingroup
  \fontsize{\@xivpt}{18}\bfseries\centering
  #1\par \endgroup
  \skip@34\p@ \advance\skip@-\normalbaselineskip
  \vskip\skip@ }
\fi
\renewcommand\section{\@startsection{section}{1}{\normalparindent}%
{14pt plus 3pt minus 2pt}{1sp}
{\normalfont\bfseries\scshape}}
%
\renewcommand\subsection{\@startsection{subsection}{2}{\normalparindent}%
{12.5pt plus 3pt minus 2pt}{1sp}
{\normalfont\scshape}}
% 
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\normalparindent}%
{12.5pt plus 3pt minus 2pt}{1sp}
{\normalfont\itshape}}
% 
\renewcommand\paragraph{\@startsection{paragraph}{4}{\parindent}%
{3mm}{-.2em}{\normalfont\scshape}}
% 
\newcommand\paragraphc{\@startsection{paragraph}{4}{\parindent}%
{3mm}{1sp}{\normalfont\scshape}}
% 
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
{3mm}{-.2em}{\normalfont\itshape}}
% 
\newcommand{\subparagraphc}{\@startsection{subparagraph}{5}{\parindent}%
{3mm}{1sp}{\normalfont\itshape}}
\renewcommand{\@seccntformat}[1]{{\upshape\csname the#1\endcsname}.\hspace{0.5em}\ignorespaces}
\def\cdr@secpoint{\pointir}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}
\numberwithin{equation}{section}
%% Biblio
\renewcommand{\@bibtitlestyle}{%
  \section*{%\kern-\parindent
\bibname}%
}
\AtBeginDocument{\@ifpackageloaded{hyperref}{}{\def\url#1{#1}%
  \def\href#1#2{#2}}}
%% Listes
\newcount\cdr@listdepth \cdr@listdepth=0
\def\cdr@list{%\begingroup%
  \ifnum \cdr@listdepth > 5\relax
   \@toodeep
  \else
   \edef\cdr@margin{cdr@leftmargin\romannumeral\the\cdr@listdepth}
   \global\advance\cdr@listdepth\@ne
  \fi
}
\def\endcdr@list{\global\advance\cdr@listdepth\m@ne\endgroup}
\def\cdr@list@indent{%
   \@tempdima=\leftskip
   \ifdim\@totalleftmargin>\z@
     \@tempdima=\@totalleftmargin\fi
   \begingroup
     \@@par\advance\@tempdima\@xp\csname\cdr@margin\endcsname
     \leftskip\@tempdima
}
\newenvironment{cdr@enumerate}[1][]{%
  \cdr@list
  \ifnum \@enumdepth >\thr@@ \@toodeep\else
   \advance\@enumdepth \@ne
\cdr@list@indent
   \edef\@enumctr{enum\romannumeral\the\@enumdepth}%
%  \@xp\leftskip\csname cdr@leftmargin\romannumeral\the\@enumdepth\endcsname
     \@ifnotempty{#1}{\@xp\def\csname cdr@the\@enumctr\endcsname{\csname #1\endcsname{\@enumctr}}}%
      \renewcommand\item[1][]{\ifvmode\else\unskip\fi\@@par\refstepcounter{\@enumctr}%
       \@ifempty{##1}{\@xp\csname cdr@label\@enumctr\endcsname{\csname cdr@the\@enumctr\endcsname}}{##1}\kern.3em\ignorespaces}%
\fi}
{\@@par\setcounter{\@enumctr}{0}\endcdr@list}
\newenvironment{cdr@itemize}[1][]{%
  \cdr@list
  \ifnum \@itemdepth >\thr@@\@toodeep\else
  \advance\@itemdepth\@ne
\cdr@list@indent
  \edef\cdr@itemitem{cdr@labelitem\romannumeral\the\@itemdepth}%
%  \begingroup
  \@ifnotempty{#1}{\def\@tmpa{#1}\def\cdr@itemitem{@tmpa}}%
%  \@@par\advance\leftskip\number\cdr@listdepth\normalparindent
%%   \advance\@tempdima\normalparindent
%%   \leftskip\@tempdima
%  \@xp\leftskip\csname  cdr@leftmargin\romannumeral\the\@itemdepth\endcsname
  \renewcommand\item[1][]{\ifvmode\else\unskip\fi\@@par
    \@ifempty{##1}{\@xp\csname\cdr@itemitem\endcsname}{##1}\kern.3em\ignorespaces}%
\fi}
{\@@par\endcdr@list}
\def\cdr@theenumi{\@arabic\c@enumi}
\def\cdr@theenumii{\@alph\c@enumii}
\def\cdr@theenumiii{\@roman\c@enumiii}
\def\cdr@theenumiv{\@roman\c@enumiv}
\def\cdr@labelenumi#1{#1.}
\def\cdr@labelenumii#1{(#1)}
\def\cdr@labelenumiii#1{\textsc{#1}.}
\def\cdr@labelenumiv#1{(#1)}
\newdimen\cdr@leftmargin
\newdimen\cdr@leftmargini
\newdimen\cdr@leftmarginii
\newdimen\cdr@leftmarginiii
\newdimen\cdr@leftmarginiv
\newdimen\cdr@leftmarginv
\newdimen\cdr@leftmarginvi
\cdr@leftmargin0pt
\cdr@leftmargini\normalparindent
\cdr@leftmarginii\normalparindent
\cdr@leftmarginiii\normalparindent
\cdr@leftmarginiv\normalparindent
\cdr@leftmarginv\normalparindent
\cdr@leftmarginvi\normalparindent
\def\cdr@labelitemi{---}
\def\cdr@labelitemii{---}
\def\cdr@labelitemiii{--}
\def\cdr@labelitemiv{--}
\AtBeginDocument{%
\let\itemize\cdr@itemize
\let\enditemize\endcdr@itemize
\let\enumerate\cdr@enumerate
\let\endenumerate\endcdr@enumerate
}
%% Floats
\def\@captionheadfont{\small}
\def\@captionfont{\small}
\def\captionseparator{\pointir}
\newskip\abovecaptionskip \abovecaptionskip=12pt \relax
\newskip\belowcaptionskip \belowcaptionskip=12pt \relax
\newdimen\captionindent \captionindent=\normalparindent
\numberwithin{figure}{section}
\numberwithin{table}{section}
\newskip\@bigflushglue\@bigflushglue-20mm plus 1fil
\newcommand{\LargeOddCentering}{\let\\\@centercr\rightskip\@bigflushglue
\leftskip\z@\parindent\z@\parfillskip\z@skip}
\newcommand{\LargeEvenCentering}{\let\\\@centercr\rightskip\z@
\leftskip\@bigflushglue\parindent\z@\parfillskip\z@skip}
\def\Largecenter{\trivlist\ifodd\c@page\LargeOddCentering\else\LargeEvenCentering\fi\item\relax}
\def\endLargecenter{\endtrivlist}

\ifx\oldpilcrowone\undefined
  \let\oldpilcrowone \P
  \let\oldpilcrowtwo \P
\fi


\endinput