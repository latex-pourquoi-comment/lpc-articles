\documentclass[french]{cedram-cg}
\usepackage[utf8]{inputenc}%Beurk, vive XeLaTeX, mort aux caractères actifs!
\usepackage[T1]{fontenc}
\usepackage[main=french,english]{babel}
\frenchsetup{StandardLayout=true}
\usepackage{hyperref}

\usepackage{biblatex}
\addbibresource{\jobname.bib}
\usepackage{filecontents}
\begin{filecontents}{\jobname.bib}
@online{samewords,
  author={Michael Stenskjær Christensen},
  title={sameword},
  url={https://pypi.python.org/pypi/samewords/}
}
@online{SE,
  author={Maïeul},
  title={eledmac/eledpar having one footnote series per \textbackslash pstart…\textbackslash pend},
  subtitle = {answer},
  url={https://tex.stackexchange.com/a/234560/7712}
}
@manual{reledmac,
  title={reledmac},
  subtitle={Typeset scholarly editions with \LaTeX},
  author={Maïeul Rouquette},
  url={http://mirrors.ctan.org/macros/latex/contrib/reledmac/reledmac.pdf},
  version={2.24.2}
}
@manual{reledpar,
  title={Parallel typeseting for critical editions: the reledpar package},
  author={Maïeul Rouquette},
  url={http://mirrors.ctan.org/macros/latex/contrib/reledmac/reledpar.pdf},
  version={2.20.2}
}
@manual{eledpar,
  title={Parallel typesetting for critical editions: the eledpar package},
  author={Maïeul Rouquette},
  url={http://mirrors.ctan.org/macros/latex/contrib/eledmac/eledpar.pdf},
  version={1.17.1}
}

@manual{eledmac,
  title={reledmac},
  subtitle={Typeset scholarly editions with \LaTeX},
  author={Maïeul Rouquette},
  url={http://mirrors.ctan.org/macros/latex/contrib/eledmac/eledmac.pdf},
  version={1.24.12},
}


@manual{ledmac,
  title={ledmac (deprecated)},
  subtitle={A presumptuous attempt to port
EDMAC, TABMAC and EDSTANZA to \LaTeX},
  author={Peter Wilson and Herries Press and Maïeul Rouquette},
  url={http://mirrors.ctan.org/macros/latex/contrib/ledmac/ledmac.pdf},
  version={0.19.4}
}
@manual{ledpar,
  author={Peter Wilson and Herries Press and Maïeul Rouquette},
  title={Parallel typesetting for critical editions: the ledpar (deprecated) package},
  url={http://mirrors.ctan.org/macros/latex/contrib/ledmac/ledpar.pdf},
  version={0.14a}
}
@book{edmac,
	author={John Lavignino and Dominik Wujastyk},
  title={Critical Edition Typesetting: The EDMAC Format for PLAIN\TeX},
  publisher={TEX Users Group and the UK TEX Users Group},
  date={1996}, 
  url={https://archive.org/details/edmac}
}
@article{edmac_overview,
	author={John Lavignino and Dominik Wujastyk},
  title={An overview of EDMAC: a Plain \TeX format for critical editions},
  journaltitle={TUGboat},
  date={1990},
  volume={11},
  number={4},
  pages={623-643},
  url={http://www.tug.org/TUGboat/Articles/tb11-4/tb30lava.pdf}
}
\end{filecontents}

\usepackage{csquotes}
\usepackage{hyperref}

\newcommand{\cs}[1]{\texttt{\textbackslash#1}}

\author{\firstname{Maïeul} \lastname{Rouquette}}
\email{maieul@maieul.net}
\title[Historique et perspectives pour les éditions critiques avec \LaTeX]{De \package{edmac} à \package{reledmac} et \package{reledpar}
\\
Historique et perspectives pour les éditions critiques avec \LaTeX}

\usepackage{metalogo}
\newcommand{\pdfLaTeX}{pdf\LaTeX}

\DeclareFontFamily{\encodingdefault}{\ttdefault}{\hyphenchar\font=`\-}


\newcommand{\package}[1]{#1}%A redefinir au besoin


\usepackage[noend,noeledsec,nofamiliar,series={A}]{reledmac}
\firstlinenum{1}
\linenumincrement{1}
\AtBeginDocument{\Xarrangement{paragraph}}
\setapprefprefixsingle{l.~}


\usepackage{listings}
\lstset{language=tex,breaklines=true,breakatwhitespace=true,captionpos=b}
\newcommand\pointmedian{%
 \kern-0.25em\textperiodcentered\kern-0.25em}
\catcode`·=\active
\def·{\pointmedian}
\begin{document}
\nocite{*}
\maketitle

L'édition critique d'un texte, ancien ou moderne, nécessite de nombreuses compétences paléographiques, philologiques et linguistiques.
 Elle impose par ailleurs des contraintes typographiques particulières;
 c’est pourquoi \TeX\ et \LaTeX\ sont parfaitement adaptés à un tel travail.

 Le principal package d'édition critique pour \LaTeX\  s'appelle aujourd'hui \package{reledmac}.
 Il est le produit des évolutions que j'ai apportées au package \package{ledmac} --- aujourd'hui déprécié --- depuis 2011.

 Le présent article se propose de parcourir l'histoire de ce package et de fournir quelques pistes pour son évolution future\footnote{Article adapté d'une présentation aux journées \LaTeX\ organisées par l'Enssib de Lyon les 11-12 septembre 2017. Je remercie Jean-Daniel Kaestli pour ses avis de philologue aguerri.}.

 
 

\section{Qu'est-ce qu'une édition critique?}

\subsection{Du problème de la transmission de textes}
Le but principal d'une édition critique est d'offrir aux lecteurs et lectrices une forme d'un texte, plus ou moins ancien, qui soit utilisable à l'époque contemporaine.
Cette forme du texte constitue le résultat d'un travail scientifique et n'est donc pas  accessible directement en consultant les supports matériels qui nous ont transmis ce texte jusqu'à l'époque contemporaine.

Un texte peut en effet nous parvenir à travers un témoin matériel unique (tablette d'argile, papyrus, parchemin, papier, etc.) posant généralement des difficultés de lecture, que celles-ci soient liées à l'état de conservation du support ou à des problèmes d'ordre paléographique.
 Pour les textes d'une époque plus récente, il peut également se poser la question des éventuelles corrections apportées à  l'œuvre, par exemple les divers remaniements présents dans un brouillon d'écrivain·e.
 
Un texte peut également être transmis par plusieurs témoins qui ont entre eux des divergences.
Jusqu'à l'invention de l'imprimerie, la seule manière de reproduire un texte --- et donc de le conserver et le diffuser ---  était de le recopier sur un nouveau support, travail complexe qui implique quasi nécessairement des modifications du texte.
 Un scribe peut ainsi être amené à confondre des homonymes, à remplacer un terme par un synonyme, à oublier des passages ou au contraire à les dupliquer, etc.
 Il peut par ailleurs apporter volontairement des retouches au texte recopié, pour le rendre plus conforme à un projet littéraire, théologique, politique, idéologique\ldots\
Même après l'invention de l'imprimerie, il n'est pas rare de voir des modifications entre diverses éditions d'une même œuvre, chaque typographe devant recomposer le texte lors d'une nouvelle impression.
 Il arrive même que deux exemplaires d'un même ouvrage parus en même temps divergent, le compositeur modifiant le texte entre deux impressions d'une même page.

 À des époques plus récentes, nous pouvons également être confrontés à des problèmes tels que la divergence entre deux brouillons d'un même travail, ou entre la version d'un roman publiée sous forme de feuilleton et la version publiée sous forme de livre.
 
\subsection{Du travail d'édition scientifique}
Le travail d'édition scientifique consiste à analyser les témoins d'une œuvre, à les comparer et à déterminer leurs relations.
Ce travail effectué, son ou sa responsable établira un texte soit par qu'il le jugera vraisemblablement le plus proche du texte originel, soit parce qu'il l'estimera le plus utilisable par des contemporain·e·s.
Pour établir ce texte, les écoles et les techniques divergent. Il existe cependant deux options principales : soit la reprise d'un témoin particulier, éventuellement corrigé,  soit l'établissement d'un texte à partir de plusieurs témoins, ce qu'on appelle une édition éclectique.
Un travail de normalisation est en outre souvent effectué, par exemple en ce qui concerne l'orthographe des mots ou la ponctuation.

Le texte ainsi constitué est accompagné de deux compléments indispensables.
 D'une part, une introduction présentant le texte, l'histoire de sa transmission et les choix effectués par l'éditeur ou l'éditrice.
 D'autre part, un \emph{apparat textuel}, ou \emph{apparat critique}\footnote{Cette seconde expression peut cependant être utilisée par des chercheur·euse·s pour désigner autre chose que le simple \emph{apparat textuel}.} en note de bas de page --- plus rarement en note de fin --- indiquant les principales variantes présentes dans les témoins anciens\footnote{Le degré de précision de l'apparat textuel dépend des choix éditoriaux et scientifiques.}, les corrections effectuées ou suggérées par l'éditeur·trice ou par ses prédécesseur·ceuse·s, les problèmes de déchiffrement des témoins, les corrections présentes dans les témoins, et, plus généralement, toutes les remarques utiles concernant la transmission ou la compréhension de tel passage précis du texte.

 L'édition critique reçoit généralement des compléments.
 Parmi les plus fréquents, citons l'apparat des sources --- indiquant les sources utilisées par le texte édité ---, les notes de commentaire historique ou philologique, la traduction dans une langue moderne, les index, les commentaires généraux et la bibliographie.
 
 

\subsection{Conséquences typographiques}

La complexité des éléments accompagnant le texte édité entraîne plusieurs nécessités typographiques, auxquelles \TeX\ et \LaTeX\ peuvent répondre.

Tout d'abord, la présence de plusieurs types d'annotations --- apparat textuel, apparat des sources, notes de commentaires --- implique de pouvoir disposer de plusieurs niveaux de note, chaque niveau correspondant à une fonction précise.

Ensuite, une notes --- quelque soit sa fonction --- renvoie à une portion du texte, et non à points précis du texte.
Par conséquent, le système habituel d'appel de notes par un numéro en exposant (ou entre crochets) est généralement inefficient.
Un tel numéro signale en effet une position dans le texte --- un point précis généralement situé entre deux mots ---  et non une portion de texte --- un ensemble de mots s'étendant entre deux points précis.
 C'est pourquoi, dans les éditions critiques, les lignes sont généralement numérotées afin de pouvoir renvoyer à un mot ou un groupe de mots,  appelé
 \emph{lemme}, situé sur une ou plusieurs lignes. La figure~\ref{figure:MWE} présente un tel rendu typographique des notes de bas de page.
 
\def\MWEedition{
  \begin{minipage}{0.85\textwidth}
    \beginnumbering
      \pstart
      Le petit \edtext{chat}{\applabel{chat}\Afootnote{chien A}} est \edtext{mort}{\applabel{mort}\Afootnote{décédé B}}.
 Il est tombé du toit.
 Pourquoi est-ce \edtext{toujours}{\applabel{toujours}\Afootnote{\emph{om.} C}} les petits chats qui meurent et jamais les papes qui tombent du \edtext{toit}{\applabel{toit}\Afootnote{\emph{add.} par terre AD}} ?
     \pend
    \endnumbering
  \end{minipage}
}
 \begin{figure}[h!]
 \centering
\MWEedition
  \caption{\label{figure:MWE}Exemple de rendu typographique d'une édition critique}
\end{figure}

Cet exemple se comprend ainsi :
\begin{itemize}
  \item le mot \enquote{chat}, situé \appref{chat} dans l'édition critique,  est remplacé  par le mot \enquote{chien} dans le témoin~A;
  \item le mot \enquote{mort} (\appref{mort}) est remplacé par le mot \enquote{décédé} dans le témoin~B;
  \item le mot \enquote{toujours} (\appref{toujours}) est absent du témoin~C;
  \item le mot \enquote{toit} (\appref{toit}) est suivi des mots \enquote{par terre} dans les témoins~A et D.
\end{itemize}

Le présent apparat est négatif, c'est-à-dire qu'il n'indique pas pour chaque variante retenue dans le texte édité le ou les témoins qui l'attestent.
Certaines collections préfèrent des apparats positifs, dans lesquels on indique également en note les témoins attestant la forme retenue, puis seulement les variantes des autres témoins\footnote{Le package \package{reledmac} et ses prédécesseurs sont neutres sur ce point}.
 Il est également fréquent d'indiquer en tête de l'apparat la liste des témoins utilisés pour la page courante, notamment lorsque certains témoins ne couvrent qu'une portion du texte\footnote{La version~2.11.0 de \package{reledmac} a ajouté des outils pour répondre à ce besoin.}


À noter que si dans le présent exemple les numéros de lignes sont systématiquement indiqués en marge, il est plus fréquent de numéroter ces lignes de~5 en~5. De même, il est fréquent de n'afficher dans l'apparat le numéro de ligne que la première fois qu'une note lui est associée.
 

Enfin, pour les textes anciens, il est généralement nécessaire d'avoir en parallèle du texte édité une traduction en langue moderne. La page de gauche est réservée au texte lui-même, tandis que la page de droite est dédiée à la traduction, ou inversement.
Il est dès lors indispensable de s'assurer de la synchronisation des pages --- sachant qu'une synchronisation parfaite est impossible, puisqu'une traduction ne compte jamais exactement le même nombre de caractères qu'un texte originel.
 
\section{De \package{edmac} à \package{ledmac} et \package{ledpar}}
Ces contraintes typographiques peuvent être résolues avec \LaTeX\ en employant les packages \package{reledmac} et \package{reledpar}.
 Ceux-ci sont les successeurs d'une série de modules Plain\TeX\ puis \LaTeX\ que j'ai pris en charge en 2011.
 

 \subsection{Le jeu de macros \package{edmac} et ses compléments}
 En 1988-1989, John~Lavignino et Dominik Wujastyk présentent le premier jeu de macros \TeX\ dédié à l'édition critique.
 Ce jeu de macros s'appelle \enquote{edmac}, abréviation de \enquote{edition macros}\cite{edmac_overview}.
 Les bases des futurs packages \LaTeX\ y sont posées : les lignes peuvent être numérotées, selon un schéma qui peut être réglé avec précision\footnote{Par exemple, faut-il afficher le numéro à chaque ligne, ou uniquement toutes les \emph{n} lignes ? Faut-il avoir numérotation continue ou recommencer la numérotation à chaque page?} les notes critiques associant lemme, numéro de ligne et annotation sont disponibles sur plusieurs niveaux.
 En 1994, on comptait treize ouvrages publiés avec \enquote{edmac}\autocite[7-8]{ledmac}.

 La figure~\ref{figure:MWE_edmac} présente un exemple d'utilisation de macros fournies par \package{edmac}.


   \begin{lstlisting}[float=h!,caption={Exemple d'édition avec \package{edmac}},label=figure:MWE_edmac]
\beginnumbering
\pstart
\text{lemme}\Afootnote{note}\
\pend
\endnumbering
   \end{lstlisting}


 On peut commenter ainsi cet exemple:
 \begin{itemize}
   \item \cs{beginnumbering} et \cs{endnumbering} délimitent la section de texte qui sera numéroté.
   \item \cs{pstart} et \cs{pend} délimitent un paragraphe numéroté\footnote{Il est possible d'utiliser la commande \cs{autopar} pour éviter d'utiliser \cs{pstart} et \cs{pend} en délimitant les paragraphes selon la méthode classique de \TeX, à savoir l'insertion d'une ligne vide, mais \cs{autopar} peut échouer dans certains cas.}.
   \item La macro \cs{text} reçoit deux arguments : d'une part un lemme, d'autre part une ou plusieurs notes associées à ce lemme.
     Ici, la note est de niveau~A (\cs{Afootnote}), mais  \package{edmac} propose déjà cinq niveaux de notes, allant de~A à~E. 
     
     On notera que \cs{text} est une macro \TeX\ et non pas une commande \LaTeX\footnote{Elle est définie par \cs{def} et non par \cs{newcommand}.}.
     Ainsi qu'il est possible dans la syntaxe de \TeX, les auteurs de \package{edmac} ont décidé de délimiter les arguments de la macro \cs{text} par une paire d'accolades pour le premier argument et une barre oblique inverse finale pour le second argument.
 \end{itemize}

 Les jeux de macros \package{edstanza} (Wayne Sullivan, 1992) pour la gestion de la poésie et \package{edtab} (Herbert Breger, 1996) pour la gestion des données tabulaires compléteront ensuite \package{edmac}.
 
 \subsection{Les packages \package{ledmac} et \package{ledpar}}

 En 2003, Peter Wilson porte \package{edmac} et ses compléments pour \LaTeX.
 Il sous-titre son travail \hyphenquote{english}{A presumptuous attempt to port EDMAC, TABMAC and EDSTANZA to \LaTeX}, que l'on peut traduire ainsi : \enquote{Une tentative présomptueuse de porter 
   EDMAC, TABMAC et EDSTANZA pour \LaTeX}.
 À titre personnel, 
 que je n'ai jamais compris ce sous-titre, puisque le travail de Peter Wilson n'avait rien de présomptueux, mais prenait simplement en compte le fait que \LaTeX\ constituait un format \TeX\ des plus répandus, et qu'il était nécessaire de pouvoir disposer d'un package d'édition critique.


 Peter Wilson intitule son portage \package{ledmac}, le \enquote{l} initial renvoyant évidemment à \LaTeX.
 Il reprend le code de \package{edmac} et l'adapte aux spécificités de \LaTeX.
 Le jeu de macro se charge désormais comme un package \LaTeX, à travers la commande \cs{usepackage}.
 Les commandes disposent désormais d'une syntaxe \LaTeX, les arguments obligatoires  délimités par une paire d'accolades et les arguments facultatifs par une paire de crochets.
 En outre, la commande \cs{label}, qui permet de renvoyer à une ligne d'édition critique, est renommée \cs{edlabel}, puisque \LaTeX\ dispose déjà d'une commande nommée \cs{label}, avec laquelle il ne faut pas entrer en conflit.

 Peter Wilson intègre également dans \package{ledmac} les fonctionnalités  de \cs{edstanza} et \cs{edtab}.
 Il ajoute également la possibilité d'indexer en renvoyant à un numéro de ligne et la possibilité d'avoir des notes dites  \enquote{familières}, c'est-à-dire avec appel de note, sur plusieurs niveaux.

 Cependant, Peter Wilson ne change pas la logique globale d'utilisation des macros, comme le montre l'exemple~\ref{figure:MWE_ledmac}, où la seule différence avec l'exemple~\ref{figure:MWE_edmac} est le passage de la macro \cs{text} à \cs{edtext} et le changement dans la délimitation des arguments\footnote{Du reste, Peter Wilson fournit dans \package{ledmac} une commande \cs{critext} qui reprend la même délimitation des arguments que la macro \cs{text} de \package{edmac} afin de faciliter la migration.}

      \begin{lstlisting}[float=h!,caption={Exemple d'édition avec \package{ledmac}},label=figure:MWE_ledmac]
\beginnumbering
\pstart
\edtext{lemme}{\Afootnote{note}}
\pend
\endnumbering
   \end{lstlisting}

Peter Wilson ne se contente pas d'adapter \package{edmac} à \LaTeX\ : en 2004, il complète \package{ledmac} avec le package \package{ledpar} qui permet de gérer la mise en  parallèle de deux textes, habituellement pour mettre en vis à vis un texte et sa traduction. Les deux textes peuvent être mis en parallèle soit sur deux colonnes, soit sur deux pages. Le respect du parallélisme se fait au paragraphe près, ce qui est un compromis satisfaisant, compte tenu de la divergence pouvant exister entre la taille du texte de gauche et celle du texte de droite. De plus, les deux packages \package{ledmac} et \package{ledpar} sont parfaitement compatibles entre eux, si bien que chacun des côtés peut recevoir ses propres notes critiques.

 Peter Wilson complète en 2003 \package{ledmac} par  \package{ledarab}. Ce package, qui se base sur \package{arabtex}, permet de gérer des éditions critiques en arabe.


\section{De \package{ledmac} et \package{ledpar} à \package{reledmac} et \package{reledpar}}

\subsection{Reprise en main de \package{ledmac} et \package{ledpar}}

De 2003 à 2005, Peter Wilson maintient bénévolement \package{ledmac} et \package{ledpar}. Toutefois, le développement s'arrête en 2005, faute de temps. Cependant, de nombreuses astuces sont publiées par diverses personnes sur internet.
En 2011, une proche connaissance me signale l'une ces astuces, qui permet de mieux gérer la mise en parallèle d'un texte en vers et de sa traduction en prose.
Il m'apparaît alors qu'il serait plus utile que cette astuce soit intégrée directement comme une option de \package{ledpar}, plutôt que proposée sur internet sous forme de surcharge du code de \package{ledpar}.

Je modifie ainsi le code de \package{ledpar} et propose la modification sur le CTAN. Les gestionnaires me signalent alors que conformément à la Licence LPPL appliquée au package, il me faut l'accord du mainteneur initial pour diffuser la nouvelle version sous le même nom de \package{ledpar}\footnote{Il s'agit d'une règle spécifique à la Licence LPPL, permettant d'assurer un minimum de compatibilité ascendante dans les modules \LaTeX.}. 
Peter Wilson accepte très volontiers de me transférer la responsabilité de \package{ledmac} et \package{ledpar}, et c'est ainsi que j'en deviens le mainteneur officiel.

Je décide alors d'ouvrir un dépôt Github afin de garder un historique du code des packages et de permettre à d'autres de suggérer des modifications en utilisant un système de tickets, plus fiables que les courriers électroniques. Les demandes commencent alors à arriver. Pendant un an, j'apporte des modifications aux deux packages. 



\subsection{Passage à \package{eledmac} et \package{eledpar}}

Au mois d'août 2012, je décide de m'attaquer à une demande récurrente, à laquelle je n'avais pu consacrer du temps auparavant : intégrer dans \package{ledmac} des astuces disponibles sur internet permettant de personnaliser l'apparence des notes de bas de page, notamment de n'afficher le numéro de ligne qu'une seule fois si plusieurs notes sont associées à la même ligne.
Outre la demande d'un utilisateur, plusieurs éléments me poussent à effectuer un tel travail. 

Tout d'abord, il me semblait qu'il s'agissait d'une fonction de base d'un logiciel d'édition critique, et qu'à ce titre une telle option devait être intégrée dans le cœur du package. Ensuite, ces astuces étaient difficilement accessibles, à la fois du fait de leurs dispersions sur différents sites internet et du fait de leur complexité en termes de programmation \TeX. Enfin, ces astuces étaient souvent incompatibles entre elles, puisque chacune d'entre elles surcharge les commandes internes de \package{ledmac} via un \cs{renewcommand}.


Je décide alors d'ajouter dans les commandes internes de \package{ledmac} des points d'entrée appellés \hyphenquote{english}{hooks} (\enquote{crochets}), qui peuvent être facilement configurés dans le préambule \LaTeX. Ainsi le code~\ref{code:hooks_eledmac} active deux hooks, \cs{numberonlyfirstinline} qui évite que le numéro de ligne soit répété entre deux notes associées à la même ligne, et \cs{symlinenum} qui permet d'afficher un symbole si le numéro de ligne n'est pas repeté\footnote{À noter que, ainsi que nous le verrons, le nom de ces hooks ont changé dans \package{reledmac}.}. Le résultat est visible sur la figure~\ref{figure:hooks_eledmac_resultat}. 

      \begin{lstlisting}[float=h!,caption={Exemple d'hooks avec \package{eledmac}},label=code:hooks_eledmac]
\numberonlyfirstinline%Dans la note de bas de page, affiche le numéro de ligne seulement si c'est la première note de la ligne
\symlinenum{$||$}%Double barre si on n'affiche pas le numéro de ligne
   \end{lstlisting}

\begin{figure}[h!]
    \Xnumberonlyfirstinline
    \Xsymlinenum{$||$}
    \centering
    \MWEedition
    \caption{\label{figure:hooks_eledmac_resultat}Résultat des hooks \package{eledmac} du code~\ref{code:hooks_eledmac}}
 \end{figure}

Chaque hook peut prendre un premier argument optionnel, entre crochets, indiquant à quelle série de notes il s'applique. 
Toutefois, afin de permettre de régler les notes série par série, j'ai dû ajouter aux commandes internes de \package{ledmac} un paramètre signalant la série concernée\footnote{Rétrospectivement, il apparaît que j'aurais pu procéder autrement.}.
Or, certaines personnes avaient redéfini ces commandes pour personnaliser \package{ledmac}, si bien que mes changements ont entraîné une rupture de compatibilité.
Sur demandes des utilisateurs et utilisatrices de \package{ledmac} et \package{ledpar}, j'ai changé le nom des packages, conformément à la philosophie de la licence LPPL qui préconise d'éviter les ruptures de compatibilités lorsqu'on charge une nouvelle version d'un package. 
C'est ainsi que \package{ledmac} et \package{ledpar} sont devenus \package{eledmac} et \package{eledpar}.
Le préfixe \enquote{e} signifie à la fois \hyphenquote{english}{extended} (\enquote{étendu}) et \hyphenquote{english}{\package{etoolbox}}, nom d'un package facilitant les développements de package \LaTeX\ que j'ai utilisé pour étendre \package{ledmac} et \package{ledpar}.

J'ai également profité de cette mise à jour pour refactoriser une partie du code de \package{ledmac}. En effet, le code interne pour les notes critiques et pour les notes familières était recopié pour chaque niveau de notes.
Outre que cela était peu pratique lorsque je devais améliorer \package{ledmac}, le fait de factoriser le code permet de rajouter facilement une nouvelle série de notes\footnote{Cela peut être utile par exemple lorsqu'on souhaite avoir une série de notes par paragraphe \autocite{SE}.}.% Il faudra que je fasse un entrée biblio, je pense. Mais pour le moment, pas de quoi tester.

En outre, j'ai profité des possibilités du moteur e\TeX\ et des nouveaux moteurs modernes (pdf\TeX, \XeTeX, Lua\TeX) --- qui peuvent gérer jusqu'à 32768~boîtes et non plus 256 ---, pour étendre le nombre de paragraphes que \package{eledpar} peut mettre en parallèle avec les réglages par défaut. 
J'ai également décidé d'abandonner la maintenance de \package{arabtex}, considérant que l'écriture en arabe ou en d'autres langues s'écrivant de droite à gauche devait être gérée au niveau des moteurs (\XeTeX\ ou \LuaTeX).

\subsection{Passage à \package{reledmac} et \package{reledpar}}

De septembre~2012 à mai~2015, j’ai amélioré régulièrement \package{eledmac} et \package{eledpar}, en ajoutant notamment de nouveaux hooks. 
Ces nouveaux hooks manquaient cependant de cohérences dans leur dénomination.
Je décide donc de les réviser entièrement en mai~2015. Or, il existait déjà une convention implicite de nommage pour les commandes de note.
\begin{itemize}
  \item Les commandes de note critique de bas de page commençaient par une lettre majuscule indiquant la série (\cs{Afootnote}, \cs{Bfootnote}, etc.). Les hooks applicables aux notes critiques commencent donc désormais par un \enquote{X} (par exemple \cs{Xnotefontsize}, pour changer la taille de la police des note).
  \item Les commandes de note familière (classique) de bas de page finissaient par une lettre majuscule indiquant la série (\cs{footnoteA}, \cs{footnoteB} etc.). Les hooks applicables aux notes familières finissent donc désormais par un \enquote{X} (par exemple \cs{notefontsizeX}).
  \item Les commandes de note critique de fin de chapitre/de volume commençaient par une lettre majuscule indiquant la série, suivi de \enquote{end} (\cs{Aendnote}, \cs{Bendnote,} etc.). Les hooks applicables aux notes critiques de fin commencent donc désormais par \enquote{Xend} (par exemple \cs{Xendnotefontsize}).
\end{itemize}

Un tel changement de nom de commandes entraînait une rupture de compatibilité, et donc un changement de nom des packages ; ceux-ci deviennent donc en juillet~2015 \package{reledmac} et \package{reledpar}, le \enquote{r} initial signifiant \hyphenquote{english}{renewed} (\enquote{renouvelé}). J'ai en effet profité de cette sortie pour relire l'ensemble du code et de la documentation des deux packages afin d'en optimiser certains points. En ce qui concerne  le code plus spécifiquement, j'ai apporté principalement trois modifications.
\begin{enumerate}
  \item J'ai uniformisé la manière de gérer certains réglages (hors hooks), en évitant de faire des \cs{renewcommand}. Désormais, la très grande majorité\footnote{Il reste encore ici ou là quelques exceptions pour des réglages de pointe.} des réglages peuvent se faire en passant un ou plusieurs  paramètres à une commande dédiée plutôt qu'en redéfinissant une commande. Cette modification avait pour but de mieux différencier ce qui relève du fonctionnement interne de \package{reledmac} / \package{reledpar} et ce qui relève des interactions avec les utilisatrices et utilisateurs.
  \item J'ai supprimé toutes les \cs{renewcommand} de \package{eledpar} qui redéfinissaient des commandes de \package{eledmac}. Désormais, si une commande s'applique à la fois à \package{reledmac} et à \package{reledpar}, elle n'est définie qu'une seule fois, ce qui facilite la maintenance et diminue les risques d'avoir un comportement différent selon que l'on soit en texte simple ou en textes parallèles.
  \item Depuis \package{edmac}, la routine de sortie --- macro appelées par \TeX\ au moment de composer une page --- était redéfinie, ce qui pouvait poser des problèmes de compatibilités avec d'autres packages. J'ai utilisé les commandes de \enquote{patchage\footnote{On nous pardonnera cet anglicisme, pour lequel, en matière de programmation, nous n'avons pas trouvé d'équivalent.}} du package \package{etoolbox} pour éviter une telle surcharge. 
    Ces commandes permettent de modifier des macro existantes en se contentant de modifier une partie de leur code interne, et non en les redéfinissant entièrement. Cela permet donc d'accroître la compatibilité entre les packages, qui peuvent ainsi éviter de redéfinir chacun la même commande.
 
 
\end{enumerate}

\section{Bilan de six ans de travail}

Au 3~septembre 2017, j'ai établi un rapide bilan de mes six années de travail bénévole sur \package{ledmac} / \package{ledpar} et leurs successeurs.

D'un point de vue statistique, on comptabilisait à cette date près de six~cents améliorations ou résolutions de bogue, et plus de deux nouvelles versions sorties, représentant plus de huit-mille \foreignlanguage{english}{\emph{commits}}%
\footnote{En gestion de code informatique, un \foreignlanguage{english}{\emph{commit}} désigne l'enregistrement d'une modification apportée au code. En principe, un \foreignlanguage{english}{\emph{commit}} est unitaire : il ne concerne qu'une fonctionnalité, ou sous-fonctionnalité. L'utilisation d'un système de suivi de version (dans mon cas Git) offre plusieurs avantages: possibilité de revenir facilement en arrière en cas d'ajout de bogue; partage simplifié du code entre plusieur·e·s dévellopeur·euse·s; gestion de plusieurs branches pour l'ajout de fonctionnalités en test; etc.}.
    
D'un point de vue qualitatif, il serait trop long d'énumérer ici toutes les modifications, qui sont du reste listées dans la documentation de \package{reledmac} et \package{reledpar}. En voici cependant un aperçu.

Étant donné le nombre de possibilités offertes par \package{reledmac} et son compagnon, je me suis efforcé de faciliter la vie des utilisateurs et utilisatrices en restructurant la documentation. J'ai ainsi mieux séparé ce qui relève du mode d'emploi des packages, ce qui relève de la documentation technique du code et ce qui relève des exemples. 
Ces derniers ont été rassemblés dans un dossier à part, et ont été rendus unitaires, selon le principe bien connu de l'\emph{exemple complet minimal} : un exemple ne présente qu'une fonctionnalité et il doit pouvoir être compilé tel quel.
En outre, je m'appuie depuis mai~2017 sur ces exemples pour effectuer des tests de régression lors des sorties d'une nouvelle version des packages.

En termes de fonctionnalités, j'ai ajouté cent-trente-sept~hooks permettant de régler finement l'apparence des notes, qu'elles soient critiques ou familières, de bas de page ou de fin. J'ai apporté la possibilité d'avoir des intertitres (\cs{section}, \cs{subsection}) dont les lignes sont numérotées, et qui peuvent donc recevoir des notes critiques. De plus, il est désormais possible de gérer la mise en parallèles d'intertitres avec \package{(r)eledpar}.
J'ai également tenté d'améliorer la cohérence entre l'emploi de \package{r·e·ledmac} seul et son emploi en combinaison avec \package{r·e·ledpar}.
Il arrivait en effet qu'une même commande ne produisent pas les mêmes résultats selon qu'elle était employé dans un texte simple ou dans un texte mis en parallèle d'un autre texte\footnote{Je pense y être globalement arrivé pour les utilisateurs et utilisatrices. En revanche, ainsi que je le présenterai, il reste encore du travail du point de vue du code.}! J'ai conçu de nouvelles options pour régler la synchronisation des textes parallèles avec \package{r·e·ledpar}. 
J'ai également ajouté un mécanisme pour gérer le problème des lemmes ambigus, qui se pose lorsqu'un même mot se trouve deux fois sur la même ligne. 
Il est désormais possible d'ajouter semi-automatiquement un numéro pour distinguer la première occurrence d'un mot sur une ligne d'une autre occurrence de ce même mot sur la même ligne\footnote{%
   Pour des raisons de performance lors de la compilation \TeX, il est nécessaire  de marquer par la commande \cs{sameword} les mots qui peuvent potentiellement poser problème. 
   Un utilisateur a cependant conçu un module Python pour ajouter automatiquement ces commandes \autocite{samewords}.}.

Enfin, j'ai amélioré la compatibilité avec certains packages et j'ai ajouté des options pour ne pas charger certaines fonctions lorsqu'elles ne sont pas nécessaires, ce qui optimise le temps de compilation. 
\section{Limites et souhaits}

Qu'en est-il du futur de \package{reledmac} et \package{reledpar}? Celui-ci, bien qu'ouvert, est conditionné par un certain nombre de limites tant personnelles que techniques.


J'accomplis mon travail sur ces deux packages sur un temps essentiellement bénévole.
 Durant mon mandat d'assistant diplômé (doctorant) à l'Institut Romand des Sciences Bibliques de l'Université de Lausanne (entre 2012 et 2017), j'ai pu utiliser un peu de mon temps de travail, considérant que je participais ainsi au développement de la recherche.
 Mon nouveau poste de chercheur en charge de l'édition des \emph{Actes de Barnabé} me laisse beaucoup moins de souplesse dans mes projets, et je ne peux plus m'occuper de ces packages qu'en dehors de mes heures de travail, donc sur mon temps personnel, qui est déjà occupé par d'autres activités.
 Le rythme de sortie de nouvelles versions va donc ralentir.
 
 Il est donc d'autant plus important à mes yeux d'impliquer d'avantages les utilisatrices et utilisateurs dans la vie du projet, en particulier sur trois points :
 \begin{itemize}

   \item la conception et la vérification du manuel --- d'autant plus que l'anglais dans lequel il est rédigé n'est pas ma langue maternelle -- ;
   \item la conception d'exemples complets minimums
   \item la résolution de problèmes simples.
 \end{itemize}

 En dehors même de la question du temps, le développement futur de ces packages se heurte à quelques limites techniques.

 Tout d'abord, afin de gérer la numérotation des lignes, \package{edmac} et ses successeurs utilisent la primitive \cs{vsplit} de \TeX, qui permet de séparer une boîte verticale en deux.
 Si cette méthode fonctionne globalement bien, elle ne peut s'appliquer aux textes \enquote{hors paragraphes classiques} tels que les titres, les listes, les images, les tableaux, etc.
 Pour chacun de ces types de données, il est nécessaire d'avoir du code spécifique, ce qui prend un certain temps et nécessite souvent de nombreux test.
 
 Par ailleurs, à la différence d'une mise en page \TeX\ classique, il peut exister avec \package{reledmac} et ses prédécesseurs un décalage conséquent entre le moment où un texte est lu par \TeX\ et le moment où il est mis en page dans le fichier de sortie.
 Ceci est particulièrement vrai dans le cas de \package{reledpar}, où le texte de gauche est lu avant le texte de droite, mais où les deux sont mis en page en alternance.
 Ceci nécessite une vigilance constante de la personne qui implémente de nouvelles fonctionnalités, notamment lorsque celles-ci manipulent des compteurs.
 
 Enfin,   le code \package{reledpar} contient une importante dette technique, qu'il est nécessaire de résoudre avant de pouvoir envisager des évolutions majeures.
 \begin{itemize}
   \item  Il existe une dissymétrie dans la définition des commandes internes utilisées pour le texte de gauche et celles utilisées pour le texte de droite:
     \begin{itemize}
       \item certaines commandes définies pour le texte de gauche s'appliquent aussi lorsqu'on édite du texte en mode \enquote{colonne unique}, sans mise en parallèle de textes;
       \item en revanche les commandes internes définies pour le texte de droite ne s'appliquent qu'au texte de droite.
     \end{itemize}
   \item Par conséquent, il existe fréquemment, une duplication du code entre les commandes internes de droite et celles de gauche.
 \item De plus, Peter Wilson n'avait envisagé la mise en parallèle que de deux textes.
 Ainsi, il existe deux variables booléennes permettant  de savoir si l’on en train de lire / de mettre en page le texte de gauche ou celui de droite.
 
 Pour envisager une mise en parallèle de plus de deux textes, il faudrait passer à un compteur ou bien à une macro. Une telle bascule nécessiterait toutefois un temps important, notamment en matière de test, afin de limiter les risques de rupture involontaire de compatibilité.
 
 \end{itemize}

 Ces limites posées, il est possible d'envisager le futur de \package{reledmac} et \package{reledpar} à moyen terme.
 Je souhaite tout d'abord apporter des améliorations \enquote{simples} aussi régulièrement que possible.
 À moyen terme, je souhaite concevoir des schémas pour expliquer le fonctionnement interne des deux packages
 afin d'ouvrir leur développement au plus de personnes possible. J'espère également multiplier les exemples complets minimum --- avec l'aide des utilisateurs et utilisatrices --- afin de limiter les ruptures de compatibilité involontaires.
 Je désire en outre me former plus théoriquement aux primitives de \XeTeX\ et de Lua\TeX\ de gestion des écritures de droite à gauche afin d'améliorer le support de cette fonctionnalité.
 

Enfin, à plus long terme, j'espère trouver un moyen de financer six mois de travail à temps plein, ou un an à mi-temps, pour résoudre la dette technique mentionnée plus haut, puis envisager une mise en parallèle automatique de plus de deux textes --- ce qui pose également un certain nombre de problèmes algorithmiques et typographiques, mais constituerait un atout majeure pour le monde \LaTeX\ face à des solutions logicielles propriétaires.
 
\printbibliography


\end{document}
