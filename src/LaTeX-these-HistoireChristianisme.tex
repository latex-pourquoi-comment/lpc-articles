\documentclass[french]{cedram-cg}
\usepackage[utf8]{inputenc}%Beurk, vive XeLaTeX, mort aux caractères actifs!
\usepackage[T1]{fontenc}
\usepackage[main=french,english]{babel}
\frenchsetup{StandardLayout=true}

\usepackage{csquotes}
\author{\firstname{Maïeul} \lastname{Rouquette}}
\email{maieul@maieul.net}
\title{Ce que \LaTeX peut apporter à des travaux historiques et philologiques}

\usepackage{metalogo}
\newcommand{\pdfLaTeX}{pdf\LaTeX}

\DeclareFontFamily{\encodingdefault}{\ttdefault}{\hyphenchar\font=`\-}


\newcommand{\lat}[1]{\emph{#1}}
\newcommand{\police}[1]{#1}
\newcommand{\package}[1]{#1}%A redefinir au besoin
\newcommand{\logiciel}[1]{#1}%A redefinir au besoin
\newcommand{\format}[1]{\textsc{#1}}% idem
\newcommand{\cs}[1]{\texttt{\textbackslash#1}}%idem
\newcommand{\extension}[1]{\foreignlanguage{english}{\texttt{.#1}}}%idem
\newcommand{\bibtype}[1]{\foreignlanguage{english}{\texttt{@#1}}}%idem
\newcommand{\bibfield}[1]{\foreignlanguage{english}{\texttt{#1}}}%idem
\newcommand{\bibkey}[1]{\foreignlanguage{english}{\texttt{#1}}}
\newcommand{\BibTeX}{\logiciel{BibTeX}}
\newcommand{\reffigure}[1]{\ref{fig:#1} p.~\pageref{fig:#1}}
\newcommand{\biblatexstyle}[1]{\emph{#1}}

\usepackage{filecontents}
\begin{filecontents*}{\jobname.bib}
@mvcollection{HistoireduChristianisme,
	Location = {Paris},
	Publisher = {Desclée},
	Subtitle = {Des origines à nos jours},
	Title = {Histoire du christianisme}}

@collection{Pietri1998,
	Crossref = {HistoireduChristianisme},
	Editor = {Luce Pietri},
	Title = {Les Églises d'Orient et d'Occident},
	Volume = {3},
	Year = {1998}}

@incollection{Maraval1998,
	Author = {Pierre Maraval},
	Crossref = {Pietri1998},
	Pages = {107-145},
	Title = {La réception de Chalcédoine dans l'empire d'Orient}}
\end{filecontents*}
\usepackage[style=verbose]{biblatex}
\addbibresource{\jobname.bib}

\usepackage{listings}
\lstset{language=tex,breaklines=true,breakatwhitespace=true}
\begin{document}
\maketitle

\section{Introduction}

Le 12 mai 2017, je soutenais ma thèse en théologie et en histoire intitulée \enquote{Étude comparée sur la construction des origines apostoliques des Églises de Crète
et de Chypre à travers les figures de Tite et de Barnabé\footnote{Sous la direction de Frédéric Amsler et Élisabeth Malamut.}}.
Il s'agit d'une thèse portant essentiellement sur l'histoire des représentations littéraires, mais ouverte à des questions plus larges qui m'ont notamment amené à étudier les traces archéologiques des dévotions à Tite et à Barnabé, ainsi que les sceaux représentant ces deux saints.
Par conséquent, j'ai dû gérer plusieurs problèmes:
\begin{itemize}
  \item une bibliographie volumineuse (cinquante-six pages);
  \item des textes en grec et en latin avec des traductions françaises en vis-à-vis;
  \item des tableaux plus ou moins longs;
  \item des images de différents types (plans, photographies).
\end{itemize}

Le travail final comporte sept-cent-soixante-dix pages, y compris les pages de garde des deux volumes. L'emploi de \LaTeX\ était donc particulièrement adapté à mon travail.

\section{Faciliter la lecture et la navigation}

Le caractère relativement volumineux de ma thèse rendait d'autant plus important de faciliter la lecture aux membres du jury, et --- potentiellement --- aux futur·es lecteurs et lectrices.
À cet égard, la qualité typographique reconnue à \TeX\ a présenté un avantage indéniable pour aider le jury à lire le travail et donc à le comprendre. Ce point a d'ailleurs été relevé par l'un des jurés durant la soutenance\footnote{Soyons honnête toutefois: le fait que j'ai écrit un livre sur \LaTeX\ a contribué à ce que cela soit relevé par le juré en question.}.
Mon texte alternant passage en caractères latins et citations en caractères grecs, j'ai choisi d'utiliser la police \police{Linux Libertine}.
J'ai choisi cette police à empattements non seulement du fait de son caractère libre\footnote{La police n'est plus maintenue sous ce nom, mais elle a été remplacée par la police \emph{Libertinus}, que j'utilise donc désormais.} --- ainsi que son nom l'indique --- mais aussi pour sa lisibilité et surtout pour le fait qu'elle contient les caractères latins et grecs sans rupture stylistique nette entre les deux, ce qui accroît la fluidité de lecture.

Par ailleurs, il était essentiel pour moi que le jury puisse se repérer facilement dans ma thèse. Pour ce faire, j'ai configuré avec \package{fancyhdr} les titres courants pour les rendre à la fois visibles et bien distincts du reste du texte. De plus, j'ai profité du package \package{hyperref} pour ajouter automatiquement dans le \format{pdf} final des signets reprenant le plan du travail et permettant d'aller directement à une section précise.

Enfin, ma thèse croise deux sujets --- le lien de Tite avec la Crète et celui de Barnabé avec Chypre --- qui alternent régulièrement. C'est pourquoi il était important de permettre facilement aux lectrices et lecteurs de retrouver les passages où j'avais parlé de tel aspect de l'un ou l'autre des sujets.
Le mécanisme de références croisées de \LaTeX\footnote{Les commandes \cs{label}, \cs{ref}, \cs{pageref} et apparentées.}, permettant de renvoyer à la page près, s'est donc avéré extrêmement utile.
Ainsi, j'ai dénombré plus de sept cents références croisées, qui permettent d'envisager ma thèse non seulement comme un ouvrage se lisant de manière linéaire selon une certaine logique, mais aussi comme une \enquote{toile} de concepts et d'analyses qui se lient et se relisent mutuellement.


\section{Gérer une bibliographie complexe}

La raison principale pour laquelle j'étais passé d'un logiciel WYSIWYG à \LaTeX\ en 2010 pour mon mémoire de master était la gestion de la bibliographie.
Très vite j'ai décidé d'utiliser le couple \package{biblatex}-\logiciel{biber} plutôt que l'historique \BibTeX.
En effet la nécessité de personnaliser mes styles bibliographiques pour les adapter à mon domaine de recherche rendait difficile l'utilisation de ce dernier.
En particulier, la syntaxe des styles \extension{bst} s'avère difficile à comprendre et à manipuler.
\lat{A contrario} la syntaxe de \emph{biblatex}, linéaire et basée sur des commandes \LaTeX\ me semblait de prime abord plus facile à utiliser.
Il s'est en outre avéré \emph{de facto} que \package{biblatex} offrait des possibilités fort utiles, voire indispensables,  à la gestion d'une bibliographie complexe telle qu'elle peut exister dans les études d'histoire et de philologie, ainsi que je vais l'exposer maintenant.

\subsection{Différence entre \BibTeX, \package{biblatex} et \logiciel{biber}}

\BibTeX\  est un logiciel auxiliaire de \LaTeX.
Il s'agit du logiciel historique de gestion de bibliographie.
Il est exécuté après la première passe de \LaTeX. Il lit dans le fichier \extension{aux} (généré par \LaTeX) l'ensemble des références bibliographiques utilisés par le travail et les met en relation avec le fichier \extension{bib} contenant la description normalisée de ces références.
Afin de mettre en forme ces références, il utilise des fichiers de style \extension{bst}. Ceci lui permet de générer alors un fichier \extension{bbl} contenant les références bibliographiques formatées. Lors de la seconde compilation, \LaTeX\ lit ce fichier \extension{bbl} et les insère dans le document final, aux endroits adéquat.

La principale difficulté de \BibTeX\  provient de la syntaxe des fichiers \extension{bst}, peu intuitive. C'est pourquoi le package \package{biblatex} a été inventé. Il s'est depuis complété avec des nombreuses fonctionnalités.
L'idée sous-jacente à ce package est que le réglage des styles bibliographiques doit pouvoir se faire en utilisant la syntaxe \LaTeX. C'est pourquoi le mécanisme est légèrement différent. Lors de la première compilation avec \LaTeX, le package \package{biblatex} note l'ensemble des références bibliographiques appelées dans le document dans un fichier \extension{bcf}, qui utilise une syntaxe XML. Le fichier \extension{XML} contient également quelques informations complémentaires tels que :
\begin{itemize}
  \item description des types et des champs bibliographiques autorisés et obligatoires,  ainsi que des héritages possibles entre entrées (voir plus-bas sur ce mécanisme);
  \item description de la méthode de tri des champs;
  \item description des méthodes de labellisation automatique des références bibliographiques ;
  \item description des paramètres des différentes commandes \cs{printbibliography} appelées dans le fichier \extension{tex}\footnote{Sur la nécessité d'avoir plusieurs bibliographies dans mon domaine, voir p.~\pageref{multibiblio}.};
  \item etc.
\end{itemize}

Puis, on passe au logiciel auxiliaire \logiciel{biber}\footnote{Pour des raisons de compatibilités historique, il est possible d'utiliser \package{biblatex} en utilisant \BibTeX, mais ceci ne permet pas de profiter de toutes les fonctionnalités.}. Celui-ci lit le contenu du fichier \extension{bcf} et du fichier \extension{bib} et produit un fichier \extension{bbl}. Ce fichier contient, dans une syntaxe \TeX, les entrées bibliographiques triées, en ayant résolu les mécanismes d'héritage. Il est également possible de demander à \logiciel{biber} de produire un rapport pour s'assurer de la cohérence de la bibliographie --- par exemple que les champs obligatoires soient effectivement présents.

Cependant, lorsque \logiciel{biber} est utilisé, les entrées contenues dans le fichier \extension{bbl} ne sont pas formatées. Ainsi, le fait que les titres d'ouvrages soient en italiques et les titres d'article entre guillemets ne se trouve pas dans ce fichier. La mise en forme se fait lors de la seconde compilation \LaTeX. À ce moment là, le package \emph{biblatex} lit le contenu du fichier \extensions{bbl}, et met en forme les entrées bibliographiques grâce à des commandes \LaTeX. La description des styles bibliographiques se trouvent dans des fichiers \extensions{cbx} et \extension{bbx}\footnote{Les premiers s'occupent de la présentation des entrées bibliographiques dans le corps du texte --- en histoire les entrées bibliographiques sont explicités au fur et à mesure, en note de bas de page, on ne se contente pas d'un numéro renvoyant à une bibliographie finale --- les seconds de la bibliographie finale. Bien sûr, il existe des mécanisme de mutualisation. Mentionnons également les fichiers \extension{bbx}, décrivant les réglages linguistiques.}, mais peuvent être modifiés dans le préambule du document \extension{tex} --- voire même après le préambule, mais cela n'est guère recommandé.

On le constate, les mécanismes d'utilisation de \BibTeX\  et \package{biblatex} + \logiciel{biber} sont en premier abord identiques (une compilation \LaTeX, puis une compilation \BibTeX ou \logiciel{Biber} puis une nouvelle compilation \LaTeX). Toutefois, les principes sous-jacents sont différents, et rendent le duo \package{biblatex} + \logiciel{Biber} plus souple et puissant que le seul \BibTeX\ historique. Et, pour ainsi dire, indispensable pour produire une bibliographie dans certains domaines de recherche. Voyons maintenant quels mécanismes spécifiques à \package{biblatex} + \logiciel{biber} j'ai utilisé dans ma thèse.

\subsection{Crossref en cascade et intelligents}

Je cite régulièrement des contributions individuelles (\bibtype{incollection}) d'ouvrages collectifs (\bibtype{collection}) eux-mêmes membres d'un corpus comportant plusieurs ouvrages (\bibtype{mvcollection}).
Le type \bibtype{mvcollection} est une innovation de \package{biblatex}, absent donc de \BibTeX. D'autre part, ce dernier propose un mécanisme d'héritage des champs bibliographiques pour le moins sommaire\footnote{Mécanisme d'héritage inexistant dans \logiciel{Zotero}, que j'aurais pu utiliser en lien avec un logiciel WYSIWYG.}.

Rappelons simplement le principe de base de ce mécanisme d'héritage: lorsqu'une entrée bibliographique constitue une sous-partie d'une entrée bibliographique plus large, le champ \bibfield{crossref} de la sous-entrée contient la clé bibliographique de l'entrée plus large.
Par conséquent, il est possible de saisir les informations une seule fois. Toutefois, avec \BibTeX, ce mécanisme est très limité:
\begin{itemize}
  \item Une entrée à laquelle une autre entrée fait référence via un \bibfield{crossref} ne peut elle-même faire référence à une autre entrée via un \enquote{crossref}. Avec \logiciel{biber} en revanche, le nombre d'héritages successifs est illimité.
  \item Les champs sont hérités sans changer de nom.
  Ainsi le champ \bibfield{title} d'une entrée \bibtype{collection} devient le champ \bibfield{title} de l'entrée \bibtype{incollection} et non pas \bibfield{booktitle} comme cela serait logique.
  Ceci oblige donc à dupliquer l'information dans le fichier \extension{bib}, ce qui limite considérablement l'intérêt du champ \bibfield{crossref}\footnote{Les logiciels de gestion de fichier \extension{bib} tels que \logiciel{JabRef} et \logiciel{BibDesk} permettent de dupliquer automatiquement ces informations,  mais c'est un pis-aller peu satisfaisant: en informatique, une information ne devrait jamais être dupliquée dans les fichiers de base, mais uniquement dans les fichiers finaux ou intermédiaires.}.
  En revanche, avec \logiciel{biber}, l'héritage se fait de manière logique et configurable, ainsi que le montre le schéma~\reffigure{heritage}.
\end{itemize}

Ainsi, mon fichier \extension{bib} contient les entrées suivantes:
\lstinputlisting{\jobname.bib}

Qui sont affichées de la manière suivante\footnote{En réalité, j'utilise plus de champs, notamment grâce au module \package{biblatex-morenames} qui me permet de définir un champ \bibfield{maineditor} pour l'entrée \bibkey{Pietri1998}, hérité du \bibfield{editor} de l'entrée \bibkey{HistoireduChristianisme}.}:
\begin{quotation}
  \fullcite{HistoireduChristianisme}

  \fullcite{Pietri1998}

  \fullcite{Maraval1998}
\end{quotation}
\begin{figure}[p]
  \centering
  \includegraphics[width=\textwidth]{heritage.pdf}
  \caption{Héritage des champs avec \logiciel{biber}}
  \label{fig:heritage}
\end{figure}
\subsection{Nouveaux types d'entrée}

\package{biblatex} et \logiciel{biber} permettent de créer des nouveaux types bibliographiques que \BibTeX.
Citons à titre d'exemple les types que j'ai créés : \bibtype{manuscripts} pour citer les manuscrits anciens (médievaux et byzantins), \bibtype{bookininproceedings} pour citer  les textes anciens édités à l'intérieur d'une contribution de colloque, \bibtype{bookinthesis} pour citer les sources anciennes éditées dans le cadre d'une thèse, etc.
Il est évident que le mécanisme de \bibfield{crossref} s'applique à ces entrées: une \bibtype{bookininproceedings} peut hériter des champs du \bibtype{inproceedings} le contenant, qui peut lui-même hériter du \bibtype{proceedings} le contenant, qui lui-même peut hériter du \bibtype{mvproceedings} le contenant.

\subsection{Formatage des références et tri de la bibliographie finale}

Lorsqu'on  fait référence à un texte, la convention dans mon domaine est de citer directement en note de bas de page sa notice bibliographique, en indiquant la ou les pages précises auxquelles on pense, et non pas d'indiquer un simple numéro renvoyant à la bibliographie finale.
S'il est possible d'utiliser certains packages pour avoir un tel résultat avec \BibTeX, \package{biblatex} propose cela nativement.
De plus, lorsqu'on utilise les styles de la famille \biblatexstyle{verbose}, les références à un ouvrage déjà citées peuvent êtres abrégées selon des conventions précises (par exemple par l'emploi d'expression latine telle que \lat{idem} ou \lat{op. cit.}).

Par ailleurs, la possibilité de modifier assez aisément l'ordre des entrées dans la bibliographie finale s'est révélé très utile: j'ai pu mettre d'abord toutes les œuvres anonymes, puis toutes celles ayant un auteur ou une auctrice classées par ordre alphabétique d'auteur puis de titre. De même en ce qui concerne les manuscrits, j'ai pu trier automatiquement par ville, puis par bibliothèque, puis par fond, puis par cote au sein d'un fond.



\label{multibiblio}Enfin, il convient que la bibliographie finale ne soit pas unitaire, mais \lat{a minima} scindée en deux : d'une part les documents produits par les sociétés qu'on étudie, qu'on appelle sources, d'autre part les études produites par les chercheurs et chercheuses.

Cependant, bien souvent, il convient de diviser encore ces deux grandes catégories.
Ainsi, grâce à \package{biblatex},   j'ai pu diviser mes sources en manuscrits ; source littéraires consacrées à Barnabé ; sources littéraires consacrées à Tite ; autres sources littéraires ; catalogues iconographiques ; catalogues sigillographiques.
J'avais même prévu une division des études utilisées en fonction des sujets. Cependant, ma co-directrice m'a demandé de renoncer à cette subdivision\footnote{Cette question de l'éventuelle division des études fait régulièrement l'objet de débat entre doctorant·es et directeur·rices de thèse, ainsi que j'ai pu le découvrir en partageant mon expérience.}. En quelques secondes, j'ai pu fusionner toutes ces études en une seule bibliographie.

%% Mettre un paragraphe sur la table des abréviations? Sur la gestion des claves?
\section{Présenter un contenu philologique}

Ma thèse ne constitue pas à proprement parler une thèse de philologie --- je n'y propose ni édition critique\footnote{L'édition critique d'un texte ancien cherche à reconstituer ce texte à partir de différents témoins --- principalement manuscrits. Le texte ainsi reconstitués reçoit un certain nombre d'annotations décrivant notamment les variantes présentes dans les différents témoins, mais aussi les sources utilisées, et toute autre annotation que l'éditeur·trice jugerait pertinent de mettre. Ces annotations sont la plupart du temps en notes de bas de page.} ni étude linguistique. En revanche, elle est constituée principalement d'une étude des représentations littéraires de Tite et de Barnabé. À ce titre, j'y ai été confronté à des problématiques typographiques que rencontrent les éditeurs et éditrices de textes.

Pour résoudre ces problématiques, j'ai utilisé les mécanismes de \XeLaTeX, ainsi que les mécanismes proposés par deux packages : \package{reledmac} et \package{reledpar}. Le premier package permet de mettre en forme une édition critique, tandis que le second, qui étend le premier, permet de mettre en parallèle un texte et sa traduction. Ces packages sont les héritiers d'une relativement longue histoire, décrite dans leurs manuels. Il se trouve que je suis depuis 2012 le principal mainteneur de ces packages.

\subsection{Caractères grecs}

La plupart de mes sources proviennent de l'Empire Byzantin, et sont par conséquent écrites en grec.
Je dispose d'une configuration clavier me permettant de saisir directement les caractères grecs en unicode, y compris les accents, esprits et autres iotas souscrits.
 Nul besoin donc de passer par une quelconque translittération.

 Afin d'afficher ces caractères de manière correcte dans le \format{pdf} final, j'ai choisi d'utiliser \XeLaTeX\ plutôt que \pdfLaTeX.
 Le premier gère en effet nativement l'unicode, alors que le second  se contente de \enquote{bidouiller} avec le système \TeX\ de \enquote{caractères actifs} pour faire comme s'il lisait de l'unicode, ce qui peut poser problème dans des cas très spécifiques, que je ne détaillerai pas ici\footnote{À noter que la question a évolué entre la soutenance de ma thèse et la publication du présent article. Pendant très longtemps, pour utiliser de l'UTF-8 avec \pdfLaTeX, il fallait charger le package \package{inputenc} dans le préambule, en lui passant l'option \verb+utf8+. Ce n'est désormais plus le cas : depuis 2018, le cœur de \LaTeX charge automatiquement --- lorsque nous n'utilisons pas des moteurs nativement unicode --- un mécanisme de gestion des caractères UTF-8. Il s'agit cependant, peu ou prou, des mêmes techniques que l'ancien package{inputenc}, avec les mêmes problèmes.}.


 \subsection{Textes mis en parallèle}
J'ai choisi, pour faciliter la vie des mes lecteur·trices, de proposer systématiquement pour les nombreux extraits de textes que je cite le texte en langue originelle --- lorsque je la connais --- et une traduction française.
 Grâce au package \package{reledpar}, les deux textes sont présentés en colonnes, avec une numérotation des lignes, sans que j'ai à me soucier de renuméroter ou d'ajuster les colonnes en cas de changement des marges, de la taille de police ou de tout autre réglage typographique.
La présence systématique des deux versions du texte a été appréciée par les juré·es.

Enfin, mes annexes contiennent plusieurs textes avec leurs traductions en vis-à-vis, le texte originel étant sur la page de gauche, le texte traduit sur la page de droite.
La synchronisation des deux pages a été assurée automatiquement par \package{reledpar}.

\subsection{Annotation complémentaire}
 J'ai pu également ajouté plusieurs types de notes grâces aux fonctionnalités de \package{reledmac}:
 \begin{enumerate}
   \item notes marginales indiquant la pagination / la linéation dans la première édition du texte ;
   \item notes renvoyant au numéro de ligne permettant de commenter aisément mes choix de traduction ;
   \item notes appelées par un numéro dans le corps du texte me permettant de citer les sources bibliques utilisées par mes textes ;
     afin de gagner de la place, ces notes --- très courtes mais nombreuses --- sont \enquote{paragraphées}, c'est-à-dire contenu dans un seul paragraphe, et non pas dans un paragraphe par note.
 \end{enumerate}

\section{Structurer le travail de rédaction}

Par ailleurs, un fichier \LaTeX\ n'étant, \emph{in fine} qu'un fichier texte entrecoupé de commandes, il est possible d'utiliser un outil de versionnement tel que \logiciel{git}. Un tel outil, utilisé habituellement par les programmeuse·eurs permet:
\begin{itemize}
  \item de garder un historique de son travail;
 \item de partager les fichiers avec d'autres personnes en s'assurant de la synchronisation.
\end{itemize}

Ma thèse étant un travail essentiellement individuel, seul le premier point m'intéressait.
 Le principe général est assez simple: à chaque modification représentant une certaine unité dans le travail --- par exemple l'ajout d'une section consacrée à un sujet particulier --- on enregistre l'état des fichiers dans un \enquote{commit}.
 Ce commit est accompagné d'un message décrivant rapidement les modifications par rapport au précédent commit.
 Par ailleurs, il est possible de poser des \enquote{tags} pour marquer les grandes étapes du travail --- par exemple l'achèvement d'un chapitre entier.

 Le fait de poser régulièrement des commits avec un message associé s'avère particulièrement utile lors de la rédaction.
 En effet, cela oblige à travailler par étape, de manière unitaire, et non pas de manière dispersée en abordant trente-six sujets en même temps.
 Cela forge une discipline d'écriture, incitant à limiter la dispersion, et donc, généralement, à gagner en efficacité.

 Par ailleurs, avec \logiciel{git}, il est possible de copier l'historique des fichiers sur plusieurs serveurs distants, et de synchroniser aisément ces différents historiques.
 C'est ainsi que j'ai pu disposer de deux sauvegardes distantes de ma thèse --- l'une hébergée chez un ami, l'autre chez l'association Framasoft --- en plus des deux sauvegardes régulières de mon ordinateur sur un disque dur externe.
 J'avais donc,  \emph{in fine}, cinq copies de ma thèse, toujours synchronisées.
 De quoi réduire sérieusement les risques de perte de mon travail.



\section{Et pour finir, ce que ma thèse a apporté à \LaTeX}

En guise de conclusion, je dirais que  \LaTeX\ m'a permis d'automatiser un certain nombre de tâches fastidieuses, ce qui a facilité  mon travail de rédaction tout en rendant plus aisée la lecture pour les membres du jury.
 Toutefois, tous les outils dont j'avais besoin n'étaient pas disponibles dans  \LaTeX\  au moment où j'ai commencé ma thèse et le fait d'avoir un outil aussi puissant que  \LaTeX\ m'a poussé à aller plus loin dans le perfectionnement bibliographique et typographique.

 C'est ainsi que j'ai dans le cadre de ma thèse j'ai amélioré  \package{reledmac} et  \package{reledpar} et j'ai créé plusieurs compléments à  \package{biblatex} permettant notamment:
 \begin{itemize}
   \item de gérer des types d'entrées bibliographiques et des champs qui n'existent pas en standard (\package{biblatex-bookinother}, \package{biblatex-morenames}, \package{biblatex-claves}, \package{biblatex-realauthor}, \package{biblatex-subseries});
   \item d'améliorer l'affichage des références bibliographiques dans certains cas particulier:
        \begin{itemize}
          \item sources anonymes (\package{biblatex-anonymous}) ;
          \item documents présents dans des ensembles plus vastes cités auparavant (\package{biblatex-opcit-booktitle});
          \item formes abrégées des titres de journaux et de collections (\package{biblatex-shortfields});
          \item divisions de sources anciennes, en plus de la pagination (\package{biblatex-source-divisions});
        \end{itemize}
 \end{itemize}

Dans la mesure du possible, j'ai essayé de publier mes \enquote{\TeX niques} sous forme de packages distribués sur le CTAN lorsque:
\begin{itemize}
  \item je pensais que mon besoin était suffisamment générique (principe de \emph{genericité});
  \item j'arrivais à distinguer clairement une fonctionnalité d'une autre (principe d'\emph{unicité}) ;
  \item j'arrivais à rendre compatible mes \TeX niques les unes envers les autres sans travail complexe de la part des utilisateur·trices (principe de \emph{compatibilité}) ;
  \item j'arrivais à produire un code permettant aux autres \LaTeX ien·ienne·s de pouvoir configurer ces besoins génériques à des cas particuliers (principe d'\emph{adaptabilité}).
\end{itemize}

J'espère ainsi que mon travail sera utile à d'autres chercheur·euses en sciences humaines et que ceux·celles-ci pourront profiter encore plus de \LaTeX\ pour mieux se concentrer sur le fond des recherches : comprendre le monde, comprendre les sociétés.

\end{document}
