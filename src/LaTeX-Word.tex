\documentclass[twoside,11pt,article,french,xetex]{memoir}
\usepackage{mes_articles_Xe}

\title{Pourquoi \LaTeX ?}
\author{Laurent Bloch}
\date{\today}

\begin{document}

\maketitle

\chapter{Efficacité comparée de \LaTeX\ et de MS-Word}

En 2014 Markus Knauff\footnote{\url{markus.knauff@psychol.uni-giessen.de}} et Jelica Nejasmic, de l'université de Gießen, ont publié dans la revue en ligne (à comité de lecture) \emph{PLoS ONE} un article intitulé \emph{An Efficiency Comparison of Document Preparation Systems Used in Academic Research and Development}\footnote{Cf. Knauff M, Nejasmic J (2014) “An Efficiency Comparison of Document Preparation Systems Used in Academic Research and Development”. PLOS ONE 9(12): e115069. doi: 10.1371/journal.pone.0115069, \url{http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0115069}} qui présente les résultats d'un banc d'essai comparatif des deux logiciels les plus utilisés pour la rédaction d'articles scientifiques, Microsoft Word et \LaTeX.

\section{Dispositif expérimental}

Le dispositif expérimental présentait toutes les garanties de sérieux : les auteurs ont recruté sur différents forums et listes de diffusion 40 chercheurs et étudiants issus de 6 universités allemandes, des deux sexes, de disciplines allant de la physique aux STAPS en passant par l'informatique, la psychologie, les mathématiques, l'ingénierie, etc. Ces participants volontaires étaient répartis en quatre groupes de 10 : débutants Word, experts Word, débutants \LaTeX, experts \LaTeX, classés selon leur nombre d'heures de pratique avec chaque logiciel.

L'expérience consistait à soumettre à chaque participant trois textes : un texte simple avec des notes en bas de page, un texte avec un tableau divisé en sous-tableaux, un texte avec des équations mathématiques. Chaque participant recevait chaque modèle (dans un ordre aléatoire) et avait trente minutes pour reproduire chacun. Les participants étaient invités à travailler sur leur propre ordinateur, dans leur environnement habituel, avec leur logiciel habituel, et pour \LaTeX\ avec leur éditeur de texte habituel.

\section{Résultats}

Le dépouillement du banc d'essai consistait à compter, respectivement, les fautes d'orthographe et de grammaire, les erreurs typographiques, et la quantité de texte que le participant avait réussi à traiter. Les auteurs ont traité et présenté ces données selon toutes les bonnes méthodes statistiques, pour les différentes sous-populations comme pour l'ensemble.

Le vainqueur est : Microsoft Word. À mon grand désespoir, et je vais essayer de réfuter ce résultat qui me semble fallacieux parce qu'il répond à un problème mal posé, à mon avis.

En attendant, il résulte de l'expérience de Markus Knauff et Jelica Nejasmic que les utilisateurs (débutants ou expérimentés) de Word vont plus vite que ceux de \LaTeX\ et commettent moins d'erreur dans toutes les catégories de l'essai. \LaTeX\ ne permet d'avoir de meilleurs résultats que dans la catégorie « formules mathématiques », ce qui est quand même logique. On notera aussi qu'à l'issue de l'expérience les utilisateurs de \LaTeX\ sont moins fatigués et ont éprouvé plus de plaisir que ceux de Word, ce qui est quand même réconfortant pour l'adepte de \LaTeX\ que je suis.

Enfin on n'est quand même pas là pour rigoler, et à la fin de leur article les auteurs nous infligent une petite leçon de morale : les travaux de recherche sont financés en grande partie par l'argent public, les chercheurs passent de 10\,\% à 30\,\% de leur temps à rédiger des articles, donc perdre du temps en utilisant \LaTeX\ est un détournement de fonds publics, sauf si on a beaucoup de formules mathématiques. Les auteurs invitent donc les revues scientifiques à interdire \LaTeX\ à leurs auteurs et à rendre Word obligatoire (ils ont signé une déclaration d'absence de conflit d'intérêts).

Je ne saurais laisser sans réponse ces assertions revêtues d'un costume scientifique mais néanmoins très discutables.

\section{Discussion}

Remarquons d'abord que des trois documents proposés en exercice, au moins deux sont difficiles. Le tableau avec ses sous-tableaux est très compliqué, je ne me vois pas le reproduire sans consulter un manuel, parce que je n'ai pas tous les jours l'occasion d'en confectionner de semblables, que ce soit avec Word ou avec \LaTeX. Quant aux équations mathématiques, même pour qui a pratiqué ces notations elles ne sont pas simples, alors j'imagine difficilement comment un spécialiste des STAPS ou de psychologie peut s'en débrouiller.

Je dois préciser qu'il y a une quarantaine d'années que j'utilise des logiciels de traitement de texte, et plus de vingt ans que j'utilise \LaTeX\ presque quotidiennement. J'ai écrit, et mis en pages moi-même, cinq livres avec \LaTeX\ et un avec LibreOffice, tous publiés par des éditeurs de la place, assez exigeants sur la forme, ce qui est assez différent de la production de documents de travail pour un groupe de collègues. Pour certains chapitres d'un de ces livres, j'ai récupéré la copie de mes co-auteurs en Google Docs pour la traduire en \LaTeX. Tout cela pour dire que si je n'ai pas eu l'occasion de monter un banc d'essai comme celui des auteurs de l'article cité ici, j'ai une expérience de long cours de l'usage de logiciels variés pour la production de documents éventuellement complexes.

C'est un peu l'histoire du lièvre et de la tortue : Word (ou ses succédanées LibreOffice et OpenOffice) donnent rapidement un résultat médiocre, avec \LaTeX\ le départ est plus laborieux parce qu'il faut commencer par le préambule et, si possible, une feuille de style, les deux assez difficiles à mettre au point, mais ensuite on est plus assuré de sa démarche, on n'a plus à se soucier de la mise en forme, et le résultat est meilleur. Sur un essai d'une demi-heure, \LaTeX\ sera toujours perdant ; cela dit, dans la vraie vie, un texte de quelque intérêt sera toujours l'objet d'un travail prolongé, auquel on reviendra plusieurs fois, et là investir dans l'apprentissage de \LaTeX\ sera très profitable. Une collègue me l'a expliqué un jour : contrairement à l'intuition, il faut donner à sa secrétaire (époque où il y avait des secrétaires) les textes courts, mais il faut confectionner soi-même les textes longs, parce que l'on aura à les retravailler.

La compétence dans l'usage de \LaTeX\ est cumulative : il y a une quinzaine d'années un de mes éditeurs (Eyrolles) a fait confectionner une feuille de style par Florence Henry pour un de mes livres, je la conserve précieusement, je lui ajoute périodiquement les perfectionnements que j'apprends, je l'ai adaptée à \XeLaTeX\ et à Bib\LaTeX\ que je ne connaissais pas à l'époque, etc. Elle est le réceptacle de ma science typographique. Rien de tel n'est vraiment possible avec Word.

En outre, tous ceux qui ont vraiment essayé savent que le plus difficile, ou du moins ce qui prend le plus de temps pour la mise en forme, ce sont l'index, la bibliographie, les références croisées. Je ne dis pas que ce soit complètement impossible avec Word, mais pour l'avoir fait (avec LibreOffice), je peux dire que j'ai amèrement regretté de ne pas avoir insisté pour rester sur \LaTeX\footnote{Pour la bibliographie, je signale au passage l'excellent système Zotero, qui permet la création d'une véritable base de données, interfaçable avec LibreOffice (je n'ai pas essayé avec Word mais on me dit que cela marche), et qui donne une sortie Bib\TeX\ ou Bib\LaTeX\ correcte. On peut aussi avoir une sortie Html, et partager la base en réseau. Un bon produit libre de la maison Mozilla.}.

Au bout du compte, l'avantage ultime de \LaTeX\ sur Word est le format de fichier. Reprendre avec Word ou LibreOffice sous Windows ou Linux un document créé dans les années 1980 avec Word 3 sur Macintosh est une entreprise, sinon totalement désespérée, du moins du domaine de la technique de pointe, et chaque nouvelle version de Word modifie le format de données, ce qui fait que si la secrétaire du labo est passée à la nouvelle version (payante), toute l'équipe est obligée de suivre.

Reprendre un document \LaTeX\ vieux de trente ans demandera sans doute certaines adaptations, mais rien de vraiment difficile. Or un vrai travail intellectuel (et n'est-ce pas là ce que l'on attend d'un chercheur ?) est fait pour durer. Word est peut-être bien pour de vagues notes de service, aussitôt mises à la corbeille que diffusées, mais pas pour un travail intellectuel sérieux.

\end{document}
%%% Local Variables:
%%% TeX-command-default: "XeLaTeX"
%%% End: