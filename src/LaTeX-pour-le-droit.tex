\documentclass[french]{cedram-cg}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{csquotes}

\title[\LaTeX\ pour l'écriture juridique]{Le fond et la forme~:\\ \LaTeX\ pour l'écriture juridique}

\author{\firstname{Flora} \lastname{Vern}}
\email{ienissei@gmail.com}

% \keywords{}


\newcommand{\pdfLaTeX}{pdf\LaTeX}

\makeatletter
\let\cdrenum\cdr@enumerate
\let\endcdrenum\endcdr@enumerate
\makeatother

\begin{document}

%\begin{abstract}
%\end{abstract}

\maketitle

L'un des principes de la science juridique est la distinction du fond et de la forme, notamment dans les questions de procédure et les conditions de validité des actes juridiques. Cette distinction essentielle gagnerait à être étendue à l'écriture juridique: il faudrait avoir, d'un côté, le fond du texte, qui exprime la compétence technique ou scientifique de son auteur; de l'autre, la forme du document, pour mettre en valeur le fond par une composition typographique soignée. Or, les logiciels de traitement de texte habituellement employés, notamment les gammes \enquote{Office}, ne permettent pas cette dissociation du fond et de la forme, de sorte que les universitaires, les praticiens et les éditeurs passent souvent un temps considérable à résoudre manuellement des problèmes de forme~-- ou renoncent~(plus souvent) à le faire, au détriment de la lisibilité du document. \LaTeX\ est un logiciel de composition typographique qui permet, justement, d'obtenir cette dissociation du fond et de la forme et d'établir un certain nombre de règles qui seront appliquées par le logiciel pour la mise en page du texte, avec la garantie structurelle d'une typographie soignée~(\ref{sect:1}). Ce logiciel permet, en outre, d'automatiser de nombreuses tâches fastidieuses comme la rédaction des index, de l'appareil bibliographique, des références législatives ou jurisprudentielles, et de faciliter jusqu'à la correction du document~(\ref{sect:2}). C'est pourquoi je souhaiterais présenter l'intérêt de \LaTeX\ pour l'écriture juridique\footnote{J'utilise \LaTeX\ pour la rédaction d'une thèse de doctorat en droit privé. Ce travail comporte plusieurs centaines de pages, de références bibliographiques, de décisions de justice et de textes législatifs ou réglementaires. Les juristes ont coutume de faire des plans en deux parties, deux sous-parties, etc., qui génèrent beaucoup de subdivisions hiérarchiques. On produit également une numérotation continue des paragraphes, utilisée dans les références internes et les deux ou trois index~(des noms, des matières et des textes). Les notes infrapaginales sont volontiers nombreuses et volumineuses, et contiennent souvent des abréviations. Le style de citation des revues, des textes législatifs et des décisions de justice varie suivant leur origine, même à l'intérieur d'un seul système juridique. Plusieurs bibliographies sont produites, par type de document, époque ou lieu de publication, et par thème.}.

\section{Un logiciel de composition typographique}
\label{sect:1}

À la manière des anciens typographes, \LaTeX\ permet de se concentrer sur la rédaction d'un manuscrit brut comprenant seulement les indications nécessaires pour que le logiciel réalise, dans un second temps, la composition de chaque page. Cela permet d'associer la cohérence de la forme~(\ref{sect:1.1}) à un travail plus soigné sur le fond~(\ref{sect:1.2}).

\subsection{La cohérence de la forme}
\label{sect:1.1}

\LaTeX\ est un logiciel permettant d'automatiser la production de documents d'une grande qualité typographique~(\ref{sect:1.1.1}) et d'en assurer la cohérence stylistique~(\ref{sect:1.1.2}).

\subsubsection{La qualité des documents}
\label{sect:1.1.1}

La composition typographique est un procédé consistant à transposer le texte sur la page, en tenant compte de diverses règles typographiques propres à en rendre la lecture agréable. Ce travail était traditionnellement réalisé par un typographe qui disposait des caractères en plomb à l'intérieur d'un cadre, les enduisait d'encre, et pressait une feuille de papier contre ceux-ci. \LaTeX\ procède d'une manière similaire: lors de la compilation du document, le logiciel récupère tous les caractères du paragraphe et les dispose de manière à former les meilleures lignes possibles, en tenant compte des contraintes typographiques. Pour ce faire, \LaTeX\ essaie différents points de césure et attribue à chaque ligne un \verb|\badness|, nombre qui tient compte des défauts d'espacement des caractères et de diverses règles typographiques exprimées sous la forme d'un \verb|penalty| ou de \verb|demetits|. Ces dernières permettent notamment de décourager les troncatures consécutives ou les lignes solitaires en début et fin de page~(veuves et orphelins). Après avoir ainsi pris tout le temps nécessaire pour déterminer la composition optimale d'un paragraphe, \LaTeX\ dispose les caractères sur la page et passe au paragraphe suivant.

Ce système diffère essentiellement de la technique employée par les logiciels classiques de traitement de texte, notamment dans les suites \enquote{Office}. Ces logiciels reposent sur l'idée d'un affichage instantané de la mise en page attendue: les caractères apparaissent à leur emplacement final au fur et à mesure qu'ils sont tapés sur le clavier; les italiques et les titres apparaissent immédiatement; les pages s'affichent directement sur l'ordinateur. Ce mode de fonctionnement, plus rassurant pour l'utilisateur, impose que le positionnement des caractères et des divers éléments de la page soient calculés en temps réel. L'instantanéité est rendue possible au prix d'approximations de calcul, le logiciel ne pouvant pas matériellement recalculer toutes les lignes du paragraphe sans désorienter complètement l'utilisateur qui recherche une certaine stabilité visuelle de son texte. C'est pour cela que ces logiciels de traitement de texte produisent des résultats typographiquement médiocres, sauf à corriger manuellement toutes les lignes. Le problème est particulièrement visible sur des textes de faible largeur.

\subsubsection{La cohérence stylistique}
\label{sect:1.1.2}

Les contraintes éditoriales et les charges graphiques des éditeurs et des universités imposent généralement un modèle de mise en page qu'il convient de respecter pour tout manuscrit. Lorsque le document est destiné à deux institutions différentes, il est nécessaire de modifier les styles, avec le risque omniprésent d'oublis et d'erreurs dans ces modifications. C'est sans compter les modifications plus subtiles, par exemple dans le style employé pour les références bibliographiques. Il est alors souvent nécessaire de modifier son texte pour se plier à de simples contraintes stylistiques, parce que les logiciels de traitement de texte mélangent le fond et la forme. \LaTeX\ permet, au contraire, d'assurer la cohérence stylistique des documents, y compris dans l'hypothèse d'une modification globale de la mise en page par modification de la \verb|classe| ou des \verb|packages| chargés dans le préambule.

\LaTeX\ utilise du texte brut, sans aucune information relative à la mise en page. Un texte qui aura été copié--collé depuis une autre source, par exemple un extrait de décision de justice ou d'article, sera automatiquement imprimé dans le même style que le corps de texte. Cela permet d'éviter les changements subtils~(ou moins subtils) dans la mise en page lorsque, par inadvertance, on a omis de supprimer tous les styles du texte incorporé au document. Il suffit, ensuite, de restituer les informations typographiques que l'on souhaite conserver. Le texte que l'on souhaite mettre en valeur, par exemple, est indiqué avec \verb|\emph{texte}|~(pour \emph{emphasis}, en anglais); l'éditeur peut ensuite définir \verb|\emph| pour produire des italiques ou des caractères gras selon ses besoins. D'autres commandes peuvent être crées pour organiser la mise en page.

Cela permet d'automatiser complètement le formatage des documents, et de ne pas introduire d'incohérences en cours de rédaction.

\subsection{Le travail sur le fond}
\label{sect:1.2}

Outre la qualité typographique des documents produits, \LaTeX\ permet de se concentrer sur le fond, plutôt que de lutter contre la forme. Ce logiciel laisse un grand choix dans l'interface~(\ref{sect:1.2.1}) et s'adapte remarquablement aux contraintes rédactionnelles~(\ref{sect:1.2.2}).

\subsubsection{Le choix de l'interface}
\label{sect:1.2.1}

\LaTeX\ ne permet pas de \enquote{voir} instantanément ce à quoi ressemblera le document final; il faut compiler le fichier \verb|.tex| et attendre que \pdfLaTeX\ produise un \verb|.pdf|. Certains logiciels permettent une prévisualisation sommaire du document, mais la plupart des gens utilisent un \enquote{éditeur de code}, susceptible d'afficher seulement du texte brut, sans aucune information de mise en page, ce qui permet notamment l'utilisation d'un logiciel de suivi de version~(Subversion, Git). Ils offrent l'avantage de la sécurité, puisque le document ne contient \emph{que} les caractères qui s'y trouvent, et ne conserve pas de traces d'anciennes versions, de notes ou de corrections qui pourraient relever du secret professionnel, contrairement aux fichiers issus des logiciels de traitement de texte.

Un peu austères à première vue, la plupart de ces éditeurs de code sont entièrement paramétrables. On peut écrire en gris foncé sur ivoire, ou en vert sur fond noir,~etc., moins agressifs pour les yeux que le noir sur blanc. La coloration syntaxique des commandes \LaTeX\ permet de les repérer facilement dans le texte: l'éditeur peut colorer automatiquement tous les \verb|\emph| en rouge ou les \verb|\cite| en bleu. Cela permet une approche dite sémantique de l'écriture, puisque l'éditeur met en valeur ce que l'on \emph{veut dire}. Aucune de ces aides visuelles n'apparaît dans le document \verb|.pdf|.

\subsubsection{L'adaptabilité du langage}
\label{sect:1.2.2}

L'une des caractéristiques essentielles de \LaTeX\, en tant que langage de programmation, est sa souplesse: n'importe qui peut modifier le comportement du logiciel pour l'adapter parfaitement à ses besoins en créant une classe \verb|.cls|~(pour les plus chevronnés), un package \verb|.sty| contenant une sorte de feuille de style, ou simplement un fichier de configuration inséré dans le préambule du document. L'écriture juridique peut en bénéficier quotidiennement.

Si l'on veut, par exemple, employer des abréviations pour la frappe, sans qu'elle apparaissent dans le document final, on peut écrire:

\begin{verbatim}
	\newcommand{\Cciv}{%
	  Code civil%
	  \index{Code civil}}
\end{verbatim}

La commande imprime les mots \enquote{Code civil} chaque fois que l'on tape \verb|\Cciv| dans le texte; avec la seconde ligne, la commande ajoute également une entrée à l'index. D'autres commandes existent dans certains packages pour gérer une liste des abréviations. Il existe, naturellement, des commandes plus complexes, susceptibles de réaliser des calculs, de convertir des nombres en toutes lettres, d'opérer selon qu'une condition est remplie ou non, de créer une liste de pièces annexes, ou de modifier l'intégralité de la mise en page. Elles sont disponibles à tous.

De cette manière, on peut paramétrer toutes les divisions hiérarchiques en usage dans la rédaction juridique. \LaTeX\ connaît déjà les parties~(\verb|\part|), les chapitres~(\verb|\chapter|), les sections~(\verb|\section|) et toutes les divisions inférieures. Il est possible de rajouter les titres et les sous-titres employés dans les thèses en décrivant ces commandes et leur comportement pour la mise en page, les tables des matières et les en-têtes\footnote{Une solution consiste à créer un compteur et une commande qui formate la nouvelle division hiérarchique, mais à l'insérer au même niveau que les chapitres dans la table des matières~(niveau zéro), pour ne pas bouleverser l'ordre des divisions standard en \LaTeX. Il reste possible d'offrir à cette nouvelle commande une typographie propre dans la table des matières.}. On pourra même imposer \enquote{Première partie} au lieu de \enquote{Partie I}.

Il est d'usage, chez les juristes, d'utiliser une numérotation continue des paragraphes tout au long du document. Avec un logiciel de traitement de texte, cette opération est généralement réalisée à la main: si l'on souhaite ajouter une paragraphe entre deux, il faut refaire la numérotation ou employer un \emph{bis}. Cette solution est parfaitement inélégante.

Sous \LaTeX, on utilisera plutôt:

\begin{verbatim}
	\newcounter{para}
	\newcommand{\para}[1]{%
	  \refstepcounter{para}%
	  \par\textbf{\thepara. #1. }%
	  \label{#1}}
\end{verbatim}

La première ligne crée un nouveau compteur qui sera utilisé pour les paragraphes. La commande \verb|\para| incrémente ce compteur à chaque utilisation, imprime le numéro du compteur et le nom donné au paragraphe~(dans cet exemple), et crée un \verb|\label| au nom du paragraphe. Si l'on écrit ensuite, dans le document: \verb|\para{Division de l'ouvrage}|, \LaTeX\ commencera un nouveau paragraphe avec: \textbf{1. Division de l'ouvrage}. Ensuite, il sera toujours possible d'obtenir le numéro de ce paragraphe, quel qu'il soit, en utilisant la commande \verb|\ref{Division de l'ouvrage}|. Si ce paragraphe était, dans la version définitive, placé en troisième et non en premier, toutes les références seraient modifiées en conséquence.

Sur le même modèle, on pourrait créer une commande \verb|\subpara| avec un second compteur pour les paragraphes employés dans les traités et manuels pour approfondir certains points, ou pour dénoter les ajouts apparus dans les éditions successives. Dans ce cas, il faut imprimer le numéro du paragraphe courant suivi d'un tiret et du numéro de sous-paragraphe, par exemple \textbf{121-4. La question de \dots} On ajoutera alors:

\begin{verbatim}
	\newcounter{subpara}[para]
	\newcommand{\subpara}[1]{%
	  \refstepcounter{subpara}%
	  \par\textbf{\thepara-\thesubpara. #1. }%
	  \label{#1}}
\end{verbatim}

\LaTeX\ s'adapte donc facilement aux besoins très spécifiques des juristes, contrairement aux logiciels de traitement de texte qui ne permettent pas d'automatiser ces tâches pourtant répétitives. Outre le gain de temps considérable, cela permet d'éviter les coquilles et les oublis, inévitables lorsque ces opérations sont réalisées à la main.

\section{Un logiciel adapté à l'écriture juridique}
\label{sect:2}

\LaTeX\ présente encore un intérêt particulier pour la gestion des références~(\ref{sect:2.1}) et la finalisation des manuscrits~(\ref{sect:2.2}) relevant de la science juridique.

\subsection{La gestion des références}
\label{sect:2.1}

\LaTeX est spécialement adapté à la gestion des données bibliographiques telles qu'elles sont employées en droit~(\ref{sect:2.1.1}), et présente également un grand intérêt pour la gestion des références aux textes et à la jurisprudence~(\ref{sect:2.1.2}).

\subsubsection{Les bibliographies générales}
\label{sect:2.1.1}

Le package Biblatex, combiné au moteur Biber, permet d'automatiser complètement la gestion des bibliographies. Une référence bibliographique pourra être signalée par \verb|\cite{Carbonnier:2004}|. Cette clé de citation correspond à une entrée dans un fichier \verb|.bib|, contenant toutes les informations éditoriales de la publication. La référence bibliographique sera formatée selon les instructions comprises dans la feuille de style, toutes les institutions n'ayant pas les mêmes préférences à ce sujet. Le style de citation est rendu sensible au contexte grâce à l'utilisation d opérateurs conditionnels~(\emph{if \dots\ then \dots\ else}). La même clef de citation, insérée à divers endroits du texte, pourra ainsi produire des résultats différents selon le contexte. En fonction des références environnantes, Biblatex choisira, par exemple:

\begin{quote}
	Jean \textsc{Carbonnier}, \emph{Droit civil}, PUF, 2004 (ou)\\
	J. \textsc{Carbonier}, \emph{op. cit.} (ou)\\
	\emph{Ibid.}
\end{quote}

Toutes les entrées citées dans le corps du texte sont automatiquement ajoutées à la bibliographie, que Biblatex permet également de subdiviser, ainsi qu'il est usuel de le faire dans la rédaction juridiques. En utilisant les filtres, il est possible de diviser la bibliographie en plusieurs sections contenant les monographies, les articles, les thèses,~etc. Cela permet d'éviter les redondances et les erreurs dans le classement alphabétique, inévitables lorsque la bibliographie est réalisée manuellement.

Les références aux revues juridiques n'emploient pas un mode de citation uniforme. On peut diviser les différentes revues selon les informations bibliographiques requises:

\begin{cdrenum}
	\item \verb|<journaltitle> <year>.<page>|
	\item \verb|<journaltitle> <year>, <part>, <number>|
	\item \verb|<journaltitle>, <date>/<volume>, <number>, <page>|
\end{cdrenum}

En utilisant le opérateurs conditionnels, on peut choisir le bon format de citation en fonction des champs renseignés dans le fichier \verb|.bib|, soit:

\begin{cdrenum}
	\item \emph{D.} 2000.123
	\item \emph{JCP G.}, 2000, doctr., 123
	\item \emph{Gaz. Pal.}, 12 janv. 2000, \no 10, p.~12~(ou)\\
	\hspace*{2.3em}\emph{Arch. phil. dr.}, 2000/4, p.~123
\end{cdrenum}

Biblatex ajoute également tous les auteurs cités à un \emph{index nominum} que l'on peut paramétrer, comme tout index, pour renvoyer aux numéros des paragraphes plutôt qu'à ceux des pages.

\subsubsection{La loi et la jurisprudence}
\label{sect:2.1.2}

L'emploi de Biblatex pour les textes de loi et les décisions de justice est plus original, car ces documents ne sont pas pris en charge dans les styles par défaut. La création du style est sans particularité. Il suffit de placer quelques virgules et éléments de formatage autour des champs employés pour ces références. Les opérateurs conditionnels permettent, à nouveau, de déduire la juridiction d'où émane la décision pour adapter le style en conséquence. Pour les textes de loi, on préférera aller vérifier directement le contenu du champ \verb|<type>| grâce à Biber. On peut ainsi ajouter à la volée des informations utiles dans le champ \verb|<keywords>|, par exemple le genre masculin ou féminin du texte. Cela permet de programmer le style pour imprimer \enquote{de la loi}, mais \enquote{du décret}, sans imposer à l'utilisateur final de renseigner ces informations. Biblatex permet également d'insérer une commande \verb|cite| au sein de certains champs d'une autre référence dans le fichier \verb|.bib|. Il est alors nécessaire de compiler le document plusieurs fois, mais cela permet de faire une référence groupée, par exemple, à la loi de ratification d'une ordonnance, ou au décret d'application d'une loi. Il est également possible de prévoir des styles bibliographiques pour es textes ou des décisions étrangers.

Les références aux décisions de justice sont suivies des références de tous les commentaires publiés à leur sujet dans les revues spécialisées. Cette liste est produite en renseignant toutes les clefs de citation des articles connexes dans le champ \verb|<related>|, spécifique à Biblatex:

\begin{quote}
	Cass., civ. 3\ieme, 2 janvier 2000, \no 98-12345: obs. X\dots, RTD civ.~2000.12; n. Y\dots, D. 2000.15.
\end{quote}

La référence de l'arrêt est, ici, suivie de deux commentaires dans des revues, une observation et une note. Inversement, on peut renseigner la clef de citation des articles en question dans le champ \verb|<related>| de chacun de ces articles\footnote{Une précaution consiste à désactiver l'usage des références connexes chaque fois que l'on est \emph{déjà} en train d'en imprimer une, pour éviter les récursions infinies.}, pour produire:

\begin{quote}
	X\dots, \emph{RTD civ.} 2000.12, obs. sous Cass., civ. 3\ieme, 2 janvier 2000, \no 98-12345.
\end{quote}

Biblatex peut produire un index séparé des textes de loi et des décisions de justice, ou les imprimer dans une bibliographie séparée, avec ou sans les articles connexes, ce qui constitue un gain de temps.

\subsection{La finalisation du manuscrit}
\label{sect:2.2}

\LaTeX\ offre, enfin, des outils parfois insoupçonnés pour finaliser un manuscrit ou préparer des documents pédagogiques. Quelques applications communes à tout travail de rédaction semblent particulièrement pertinentes pour l'écriture juridique: il s'agit de l'instrumentalisation des index~(\ref{sect:2.2.1}) et des tables des matières~(\ref{sect:2.2.2}).

\subsubsection{L'instrumentalisation des index}
\label{sect:2.2.1}

La plupart des logiciels de traitement de texte disposent d'un mode \enquote{correcteur} dans lequel les modifications apparaissent en couleurs jusqu'à leur approbation finale. \LaTeX\ dispose d'un système un peu plus rudimentaire. Outre les packages existants, il suffit schématiquement de créer une commande \verb|\changed{<ancien>}{<nouveau>}| qui imprime seulement l'un des deux textes, ou qui barre le premier et met le second en vert. L'intérêt, toutefois, est la portabilité du document qui contient explicitement les modifications à effectuer. On ne risque pas de les perdre en faisant relire le document par quelqu'un qui utilise le logiciel concurrent, ou une version antérieure du même logiciel. Il est également possible de créer une commande \verb|\todo{<texte>}|, qui imprime le texte en rouge, mais que l'on prendra le soin de masquer dans la version finale.

De même, \LaTeX\ permet d'employer les index à des fins de corrections. On pourrait envisager d'indexer automatiquement les noms des paragraphes pour supprimer les doublons. On peut également indexer tous les articles de code cités, ou toutes les pages citées dans certains ouvrages, afin de rechercher des redondances dans le texte.

\subsubsection{L'instrumentalisation des tables}
\label{sect:2.2.2}

Les tables des matières peuvent aussi être instrumentalisées. \LaTeX\ permet d'imprimer des paragraphes entiers de texte dans la table des matières. On peut donc imaginer la création d'une commande qui imprime le texte normalement sur la page, mais qui en insère une copie dans la table des matières\footnote{Il est nécessaire de neutraliser localement toutes les commandes qui sont incompatibles avec la table des matières, comme les listes ou les notes de bas de page. On peut également utiliser un environnement, avec le package \texttt{environ}.}. Le code serait le suivant:

\begin{verbatim}
	\newcommand{\summary}[1]{%
	  #1
	  \bgroup
	    \let\footnote\@gobble
	    \addtocontents{toc}{#1}
	  \egroup}
\end{verbatim}

La commande imprime d'abord le texte normalement, là où elle est employée, puis elle supprime localement les notes de bas de page et imprime alors le texte dans la table des matières.

Cette méthode permet d'imprimer les chapeaux des différentes sections directement dans la table des matières, et de lire le manuscrit tel que le verront ceux des lecteurs qui se contentent des têtes de chapitres. Cela permet également de produire, en même temps que le manuscrit, une forme condensée des points importants pour le relecteur.

\end{document}