LaTeX, pourquoi / comment -- Articles
=====================================

*Date de création* : 2017/06/12


Destinataires du projet
-----------------------
Ce dépôt se veut ouvert à toute personne désirant participer à la publication de ce texte promouvant l'utilisation de LaTeX pour la PAO.


Ce que le projet fait
---------------------
Gérer le code source des documents "LaTeX, Pourquoi?" et "LaTeX, Comment?" de manière participative.



Prérequis
---------
### Matériels
- Doit fonctionner sur toutes les plateformes.


### Logiciel
- LaTeX


Documentation
-------------

1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
3. [CREDITS](CREDITS) liste des contributeurs du projet
4. [HISTORY](HISTORY.md) histoire du projet
5. [LICENSE](LICENSE) termes de la licence


### Documentation généraliste
#### Git
- http://geekographie.maieul.net/83
- http://geekographie.maieul.net/191

#### GitFlow


#### LaTex



[comment]: # ( Local Variables: )
[comment]: # ( coding: utf-8    )
[comment]: # ( End:             )
