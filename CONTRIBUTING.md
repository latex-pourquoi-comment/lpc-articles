Guide de contribution
=====================

WorkFlow
--------
Ce projet utilise Git-flow au pied de la lettre:
* http://nvie.com/posts/a-successful-git-branching-model/

L'article de base qui donnera naissance au projet


* https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html

Aide mémoire français (et en d'autre traduction).


Contributions
-------------
Libre à vous de cloner le dépôt... Et de proposer des modifications.


Conventions de nomnage
======================

Arborescence de fichier
-----------------------

### doc
Placez dans *doc* et ses sous-répertoires toute la documentation afférente au projet, sans oublier les notes et courriers électroniques importants. Vous pouvez avoir des sous-répertoires de doc contenant différents types de documents ou pour différentes phases du projet.

Si vous avez besoin de documentation externe, envisager de la copier ici. Cela rendra service pour maintenir le projet si l'endroit où les données en questions étaient accessibles disparaît.


### src
Ce répertoire contient le code source du projet. Vous pouvez y faire des sous-répertoires pour différents types de code source, par exemple:

* src/inc
* src/img
* ...


### util
Répertoire contenant les utilitaires, outils et scripts spécifiques au projet.


### vendor
Si le projet utilise des bibliothèques fournies par une partie tierce ou des fichiers d'en-têtes que vous désirez archiver avec votre code, faites-le ici.


Gestionnaire de version
-----------------------
Le workflow git suit scrupuleusement git-flow.


### Branche **master**
Elle représente le dernier état installable en production du projet. Seul les administrateurs du dépôt peuvent travailler dans cette branche.


### Branche **devel**
La branche où est récolté le travail de tout le monde, des branches de développement privées. Seul la "Team" peut travailler dans cette branche.


### les branches **feature**
Chaque branche doit être Nommée de la manière suivante:

* PSEUDO-DESCRIPTION

où:

* **PSEUDO** est le pseudo de l'administrateur (le créateur) de la branche
* **DESCRIPTION** Une description en CamelCase (RaisonCreationBranche) de cette branche



[comment]: # ( Local Variables: )
[comment]: # ( coding: utf-8	)
[comment]: # ( End:		)

				