Changelog
=========

* http://keepachangelog.com/fr/0.3.0/


[Unreleased]
------------

### Added


### Changed


### Deprecated


### Removed


### Fixed


### Security




[V.0.0_original] - 2017-06-12
-----------------------------
### Added
- Fichier original fournit sur le site de l'auteur.
- Configuration du dépôt.


[comment]: # (  Local Variables: )
[comment]: # (  coding: utf-8	 )
[comment]: # (  End:		 )
